﻿using RemoteController_client.app.hook_utils;
using RemoteController_server.Debug;
using RemoteController_server.model.implementations.clipboard_utils;
using RemoteController_server.model.implementations.marshalling_utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RemoteController_client.app
{
    public enum KeyBindingsType { CTRL = 0, ALT = 1, CTRLALT = 2 };

    class RemoteClient
    {
        private string localStoragePath;

        private ConnectionsManager connectionsManager;
        private KeyboardHandler_client keyboardHookManager;
        private MouseHandler_client mouseHookManager;
        private Form1 clientForm;
        private AppData clipboardData;
        private FileManager fileManager;
        private string dropList;
        public string password;

        //Setting Options
        private KeyBindingsType switchFocusCommand;
        private int stopHookCommandIndex;
        private int remotePasteCommandIndex;
        private int stopHookCommandModifier;
        private int remotePasteCommandModifier;

        public ConnectionsManager ConnectionsManager { get { return connectionsManager; } }
        public Form1 ClientForm { get { return clientForm; } }

        public KeyBindingsType SwitchFocusCommand { get { return switchFocusCommand; } set { switchFocusCommand = value; } }
        public int StopHookCommandIndex { get { return stopHookCommandIndex; } set { stopHookCommandIndex = value; } }
        public int RemotePasteCommandIndex { get { return remotePasteCommandIndex; } set { remotePasteCommandIndex = value; } }
        public int StopHookCommandModifier { get { return stopHookCommandModifier; } set { stopHookCommandModifier = value; } }
        public int RemotePasteCommandModifier { get { return remotePasteCommandModifier; } set { remotePasteCommandModifier = value; } }
        public AppData ClipboardData { get { return clipboardData; } set { clipboardData = value; } }
        public string LocalStoragePath { get { return localStoragePath; } }
        public string DropList { get { return dropList; } set { dropList = value; } }
        public string Password { get { return password; } set { password = value; } }

        public RemoteClient(Form1 cf)
        {
            clientForm = cf;
            localStoragePath = Path.GetFullPath(".\\..\\..\\localStorage\\");
            connectionsManager = new ConnectionsManager(this);
            switchFocusCommand = KeyBindingsType.CTRL;

            keyboardHookManager = new KeyboardHandler_client(this,connectionsManager);
            mouseHookManager = new MouseHandler_client(this,connectionsManager);
            fileManager = new FileManager();
        }
       

        public bool StartHookListening()
        {
            if (!keyboardHookManager.StartListeningHook())
                return false;
            if (!mouseHookManager.StartListeningHook())
            {
                keyboardHookManager.Stop();
                return false;
            }
            return true;
        }
        public void StopHookListening()
        {
            if (keyboardHookManager != null)
                keyboardHookManager.Stop();
            if (mouseHookManager != null)
                mouseHookManager.Stop();
            clientForm.FullScreen(false);
        }

        internal async void SendRemotePaste()
        {

            RemoteConnection_client conn = connectionsManager.getFocusedConnection();
            if (!conn.IsConnected || !conn.ClipboardToSend)
            {
                Logger.Clog("Non invio perchè non è connesso o ho già inviato");
                return;
            }
            Logger.Clog("ricevuta richiesta invio clipboard");
            fileManager.Sender = conn.ClipboardConnection;
            switch (clipboardData.Type)
            {
                case DataType.UnicodeText:
                case DataType.ASCIIText:
                case DataType.Bitmap:
                case DataType.Music:
                    conn.ClipboardConnection.SendClipboardData(clipboardData);
                    break;
                case DataType.DropList:
                    Logger.Clog(">>> BASEFILE: " + dropList);

                    string[] paths = FileManager.FromBaseNameToRPaths(dropList);

                    foreach (string path in paths)
                    {
                        await fileManager.ManagePathAsync(Path.Combine(localStoragePath, path));
                    }
                    AppData data = new AppData();
                    data.Type = DataType.DropList;
                    data.Content.Clipboard.Filename = dropList;
                    conn.ClipboardConnection.SendClipboardData(data);
                    break;
                default:
                    return;
            }
            conn.ClipboardToSend = false;
            conn.GraphicalSetDownloadIcon(false);

        }

        internal void RemoveFileChunks()
        {
            fileManager.removeAllChunks(localStoragePath);
        }
    }
}
