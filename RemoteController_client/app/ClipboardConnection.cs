﻿using RemoteController_server.Debug;
using RemoteController_server.model.implementations.marshalling_utils;
using RemoteController_server.model.interfaces;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RemoteController_client.app
{
    class ClipboardConnection : IClipboardSender
    {
        private TcpClient client;
        private NetworkStream stream;
        private bool isConnected = false;
        private IFormatter formatter;
        private RemoteConnection_client conn;
        private string server;
        private bool FileIsCorrupted = false;

        public bool IsConnected { get { return isConnected; } }

        public ClipboardConnection(RemoteConnection_client conn, String server, Int32 port)
        {
            this.conn = conn;
            this.server = server;
            {
                try
                {
                    client = new TcpClient(server, port);
                    isConnected = true;
                    stream = client.GetStream();
                    formatter = new BinaryFormatter();
                    Logger.Clog("Clipboard: Connesso");
                }catch(Exception e)
                {
                    Logger.Clog("indirizzo IP non raggiungibile");
                    throw e;
                }
            }
        }
        public void SendClipboardData(AppData data)
        {
            //todo se errore serializzazione chiudere il canale!
            try
            {
                if (client.Connected)
                    formatter.Serialize(stream, data);
            }
            catch (IOException)
            {
                Logger.wr("Server has closed the connection.");
                //Disconnect();
                //conn.GraphicalDisconnect();
                conn.Disconnect();
            }
            catch (SerializationException)
            {
                Logger.wr("Server has closed the connection.");
                conn.Disconnect();
            }
            catch (Exception)
            {
                Logger.wr("General Exception");
                conn.Disconnect();
            }
        }

        public Task ReceiveAsync()
        {
            return Task.Run(() => {

                while (true)
                {
                    try
                    {
                        AppData data = (AppData)formatter.Deserialize(client.GetStream());
                        if (data.Type == DataType.Stop)
                        {
                            Logger.Clog("CLIPBOARD: Ricevuta richiesta di chiudere connessione dal server!");
                            this.Disconnect();
                            conn.ConnectionManager.App.RemoveFileChunks();
                            conn.GraphicalDisconnect();
                            return;
                        }
                        this.ElaboratePacket(data);
                    }
                    catch (IOException)
                    {
                        //Ci posso entrare se aspetto su connessione chiusa!
                        Logger.Clog("Connection Closed");
                        //this.Disconnect();
                        //conn.GraphicalDisconnect();
                        conn.Disconnect();
                        return;
                    }
                    catch (SerializationException)
                    {
                        Logger.Clog("Serialization Error");
                        conn.Disconnect();
                        return;

                    }
                    catch (DecoderFallbackException e)
                    {
                        Logger.Clog("Connection Closed");
                        conn.Disconnect();
                        return;
                    }
                    catch (Exception)
                    {
                        Logger.Clog("Connection Closed");
                        conn.Disconnect();
                        return;
                    }
                }
            });
        }

        internal void SendPassword(string password)
        {
            AppData confData = new AppData();
            confData.Type = DataType.Configuration;
            confData.Content.Clipboard.Password = password;
            SendClipboardData(confData);
        }

        internal Task SendClipboardDataAsync(AppData data)
        {
            return Task.Run(()=>
            {
                try
                {
                    if (client.Connected)
                        formatter.Serialize(stream, data);
                }
                catch (IOException)
                {
                    Logger.wr("Server has closed the connection.");
                    //Disconnect();
                    //conn.GraphicalDisconnect();
                    conn.Disconnect();
                }
                catch (SerializationException)
                {
                    Logger.wr("Server has closed the connection.");
                    conn.Disconnect();
                }
                catch (Exception)
                {
                    Logger.wr("Server has closed the connection.");
                    conn.Disconnect();
                }

            });
        }
        internal void SendFocus(bool taken)
        {
            AppData data = new AppData();
            data.Type = DataType.FocusOnYou;
            if (taken)
            {
                data.Content.Clipboard.Size = 1;
            }
            else
            {
                data.Content.Clipboard.Size = 0;
            }
            SendClipboardData(data);
        }
        internal async void SendFocusAsync(bool taken)
        {
            AppData data = new AppData();
            data.Type = DataType.FocusOnYou;
            if (taken)
            {
                data.Content.Clipboard.Size = 1;
            }
            else
            {
                data.Content.Clipboard.Size = 0;
            }
            await SendClipboardDataAsync(data);
        }

        public void SendDisconnectionRequest()
        {
            if (stream != null && client.Connected)
            {
                AppData req = new AppData();
                req.Type = DataType.Stop;
                formatter.Serialize(stream, req);
            }

        }

        public void Disconnect()
        {
            // Close everything.
            stream.Close();
            client.Close();
            isConnected = false;
            Logger.Clog("Disconnesso");
        }

        private void ElaboratePacket(AppData data)
        {
            bool isToSend = true;
            int tryAgain = 0;


            string path;
            string tmpPath;

            switch (data.Type)
            {
                case DataType.Configuration:
                    Logger.Clog("Mouse port: " + data.Content.Configuration.mouseConnPort + ", Keyboard port: " + data.Content.Configuration.keyboardConnPort);
                    conn.KeyboardConnection = new KeyboardConnection_client(conn, this.server, data.Content.Configuration.keyboardConnPort);
                    conn.MouseEndPoint = new IPEndPoint(IPAddress.Parse(this.server), data.Content.Configuration.mouseConnPort);

                    conn.KeyboardConnection.ReceiveAsync();

                    conn.IsConnected = true;
                    conn.ConnectionManager.SetFocus(conn);
                    conn.GraphicalConnect();
                    conn.App.SetLoadingCursor(false);
                    isToSend = false;
                    break;
                case DataType.UnicodeText:
                    Console.WriteLine("unicode >>" + Encoding.Unicode.GetString(data.Content.Clipboard.Data));
                    break;

                case DataType.Music:
                    Console.WriteLine("ricevuto music");
                    break;

                case DataType.ASCIIText:
                    //Console.WriteLine("ricevuto text");
                    Console.WriteLine("server>> " + Encoding.Default.GetString(data.Content.Clipboard.Data));
                    break;


                case DataType.Bitmap:
                    MemoryStream ms = new MemoryStream(data.Content.Clipboard.Data);
                    Bitmap image = new Bitmap(Image.FromStream(ms));
                    //image.RotateFlip(RotateFlipType.Rotate180FlipNone);
                    //conn.ConnectionManager.App.ClientForm.SetClipboardData(DataFormats.Bitmap, image);
                    //Console.WriteLine("Ho inserito image in cliboard");
                    break;

                case DataType.File:

                    path = Path.Combine(conn.ConnectionManager.App.LocalStoragePath, data.Content.Clipboard.Filename);
                    tmpPath = path + ".tmp";

                    tryAgain = 0;
                    while (tryAgain < 3)
                    {
                        try
                        {
                            Console.WriteLine("ricevuto file: " + data.Content.Clipboard.Filename);
                            File.WriteAllBytes(path, data.Content.Clipboard.Data);
                            break;
                        }
                        catch (IOException ee)
                        {
                            Logger.Clog("Error while writing file " + path + ".\nMessage:\n" + ee.Message);
                            tryAgain++;
                            Thread.Sleep(500);
                        }
                        catch (Exception ee)
                        {
                            Logger.Clog("Error while writing file " + path + ".\nMessage:\n" + ee.Message);
                            break;
                        }
                    }
                    break;


                case DataType.Directory:
                    path = Path.Combine(conn.ConnectionManager.App.LocalStoragePath, data.Content.Clipboard.Filename);
                    tmpPath = path + ".tmp";

                    tryAgain = 0;
                    while (true)
                    {
                        try
                        {
                            if (!Directory.Exists(path))
                            {
                                Directory.CreateDirectory(path);
                                Console.WriteLine("server>> Create directory " + path);
                            }
                            break;
                        }
                        catch (IOException)
                        {
                            if (tryAgain >= 3)
                            {
                                Logger.Clog("Error while creating directory " + path);
                                break;
                            }
                            else
                            {
                                tryAgain++;
                                Thread.Sleep(500);
                            }
                        }
                        catch (Exception ee)
                        {
                            Logger.Slog("Error while writing directory " + path + ".\nMessage:\n" + ee.Message);
                            break;
                        }
                    }
                    break;


                case DataType.FileChunk:
                    path = Path.Combine(conn.ConnectionManager.App.LocalStoragePath, data.Content.Clipboard.Filename);
                    tmpPath = path + ".tmp";

                    tryAgain = 0;
                    while (true)
                    {
                        try
                        {
                            using (var fileStream = new FileStream(tmpPath, FileMode.Append))
                            {
                                fileStream.Write(data.Content.Clipboard.Data, 0, data.Content.Clipboard.Data.Length);
                            }
                            break;
                        }
                        catch (IOException ee)
                        {
                            if (tryAgain >= 3)
                            {
                                FileIsCorrupted = true;
                                Logger.Slog("Error while writing file " + path + ".\nMessage:\n" + ee.Message);
                                break;
                            }
                            else
                            {
                                Console.WriteLine("NON VA");
                                tryAgain++;
                                Thread.Sleep(500);
                            }
                        }
                        catch (Exception ee)
                        {
                            FileIsCorrupted = true;
                            Logger.Clog("Error while writing file " + path + ".\nMessage:\n" + ee.Message);
                            break;
                        }
                    }
                    break;


                case DataType.FileChunkEnd:
                    path = Path.Combine(conn.ConnectionManager.App.LocalStoragePath, data.Content.Clipboard.Filename);
                    tmpPath = path + ".tmp";

                    if (FileIsCorrupted || data.Content.Clipboard.Size == -2)
                    {
                        try
                        {
                            if (File.Exists(tmpPath))
                            {
                                File.Delete(tmpPath);
                            }
                            break;
                        }
                        catch (Exception)
                        {
                            ///Il file è corrotto ma non posso rimuoverlo.
                            ///C'è poco da fare... 
                        }
                        finally
                        {
                            FileIsCorrupted = false;
                        }

                    }
                    else
                    {
                        try
                        {
                            using (var fileStream = new FileStream(tmpPath, FileMode.Append))
                            {
                                fileStream.Write(data.Content.Clipboard.Data, 0, data.Content.Clipboard.Data.Length);
                            }
                            if (File.Exists(path)) {
                                File.Delete(path);
                            }
                            File.Move(tmpPath, path);
                            break;
                        }
                        catch (Exception)
                        {
                            ///Il file è sempre stato ok, all'ultimo chunk si è corrotto.
                            ///provo a chiuderlo ma se non riesco c'è poco da fare.
                            try
                            {
                                if (File.Exists(tmpPath))
                                {
                                    File.Delete(tmpPath);
                                }
                                break;
                            }
                            catch (Exception) { }
                        }
                    }
                    break;
                case DataType.DropList:
                    Logger.Clog("droplist: " + data.Content.Clipboard.Filename);
                    conn.ConnectionManager.App.DropList = data.Content.Clipboard.Filename;
                    break;


                default:
                    isToSend = false;
                    break;
            }
            if (isToSend)
            {
                conn.ConnectionManager.App.ClipboardData = data;
                conn.ConnectionManager.SetAllClipboardToSend();
            }
        }
    }
}
