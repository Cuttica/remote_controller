﻿using RemoteController_server.model.implementations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace RemoteController_client
{
    class MouseConnection_client
    {
        /*
        private static UdpClient mouseConn = null;
        
        public static void SendMouseHook(INPUT packet, IPEndPoint destination)
        {
            if(mouseConn == null)
                mouseConn = new UdpClient();
            int size = 0;
            byte[] bytes = InputManager.getBytesFromInput(packet, ref size);
            mouseConn.SendAsync(bytes, size, destination);

        }
        */
        private static UdpClient mouseConn = null;

        public static void SendMouseHook(INPUT packet, IPEndPoint destination)
        {
            if (mouseConn == null)
                mouseConn = new UdpClient();
            int size = 0;
            byte[] bytes = InputManager.getBytesFromInput(packet, ref size);
            mouseConn.SendAsync(bytes, size, destination);
        }
    }
}
