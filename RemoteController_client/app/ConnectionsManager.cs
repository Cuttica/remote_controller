﻿using RemoteController_server.Debug;
using RemoteController_server.model.implementations.marshalling_utils;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RemoteController_client.app
{

    class ConnectionsManager
    {
        
        private ArrayList connList;
        private int focusIndex;
        public const int MAXCONNECTIONS = 10;
        private RemoteClient app;

        public bool isThereAnyFocusedServer { get { return focusIndex != -1; } }
        public RemoteClient App { get { return app; } }

        /// <summary>
        /// Return the first free place on the connections queue.
        /// -1 if there isn't free place
        /// </summary>
        /// <returns></returns>
        public int getFreeIndex()
        {
            int freeIndex = connList.Count;
            if (freeIndex == MAXCONNECTIONS)
            {
                return connList.IndexOf(null);
            }
            return freeIndex;
        }


        public ConnectionsManager(RemoteClient app)
        {
            connList = new ArrayList(MAXCONNECTIONS);
            focusIndex = -1;
            this.app = app;
        }

        public void AddConnection(RemoteConnection_client conn)
        {
            if (connList.Count != MAXCONNECTIONS)
            {
                Logger.Clog("Add a connection!");
                connList.Add(conn);
                //if(connList.Count == 1)
                //{
                //    setFocus(0);
                //}
            }
        }

        public void RemoveConnection(RemoteConnection_client conn)
        {
            Logger.Clog("removing a connection!");
            connList.Remove(conn);
        }

        internal void SetAllClipboardToSend()
        {
            foreach(RemoteConnection_client conn in this.connList)
            {
                conn.ClipboardToSend = true;
                conn.GraphicalSetDownloadIcon(true);

            }
        }

        //public void CloseRemoteConnection(int index)
        //{
        //    if (index < 0 || index > connList.Count)
        //        return;
        //    if (connList[index] == null)
        //        return;
        //    ((RemoteConnection_client)connList[index]).CloseConnection();
        //    connList.Remove(connList[index]);
        //    focusIndex--;
        //    return;
        //}

        public RemoteConnection_client getFocusedConnection()
        {
            return (RemoteConnection_client) connList[focusIndex];
        }

        public bool SetFocus(int index)
        {
            if (index >= connList.Count) {
                return false;
            }
            if (focusIndex != -1 && 
                (RemoteConnection_client)connList[focusIndex] != null && 
                ((RemoteConnection_client)connList[focusIndex]).IsConnected)
            {
                ((RemoteConnection_client)connList[focusIndex]).ClipboardConnection.SendFocus(false);
            }
            focusIndex = index;
            if (focusIndex != -1 &&
                (RemoteConnection_client)connList[focusIndex] != null &&
                ((RemoteConnection_client)connList[focusIndex]).IsConnected)
            {
                ((RemoteConnection_client)connList[focusIndex]).ClipboardConnection.SendFocus(true);
            }
            app.ClientForm.SetFocus(index);
            return true;
        }
        public bool SetFocus(RemoteConnection_client conn)
        {
            int index = connList.IndexOf(conn);
            if(index == -1)
            {
                return false;
            }
            //focusIndex = index;
            //app.ClientForm.SetFocus(index);
            SetFocus(index);
            return true;

        }
        
    }
}
