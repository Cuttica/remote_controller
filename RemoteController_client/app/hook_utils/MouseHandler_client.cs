﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using RemoteController_server.model.implementations.hook_utils;
using RemoteController_server.model.implementations;
using System.Net;
using RemoteController_server.Debug;

namespace RemoteController_client.app.hook_utils
{   
    class MouseHandler_client
    {

        public const int WH_MOUSE = 7;
        
        //Declare the hook handle as an int.
        static int hHook = 0;

        public delegate int HookProc(int nCode, IntPtr wParam, IntPtr lParam);

        //Declare MouseHookProcedure as a HookProc type.
        HookProc MouseHookProcedure;
        private RemoteClient client;


        //This is the Import for the SetWindowsHookEx function.
        //Use this function to install a thread-specific hook.
        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern int SetWindowsHookEx(int idHook, HookProc lpfn, IntPtr hInstance, int threadId);

        //This is the Import for the UnhookWindowsHookEx function.
        //Call this function to uninstall the hook.
        [DllImport("user32.dll", CharSet = CharSet.Auto,
         CallingConvention = CallingConvention.StdCall)]
        public static extern bool UnhookWindowsHookEx(int idHook);

        //This is the Import for the CallNextHookEx function.
        //Use this function to pass the hook information to the next hook procedure in chain.
        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern int CallNextHookEx(int idHook, int nCode, IntPtr wParam, IntPtr lParam);

        [DllImport("user32.dll")]
        private static extern bool UnhookWindowsHookEx(IntPtr hhk);

        //private IPEndPoint destination;
        private ConnectionsManager focusController;

        private bool toSend;

        public int MouseHandlerProcedure(int nCode, IntPtr wParam, IntPtr lParam)
        {
            if (nCode < 0 || !focusController.isThereAnyFocusedServer)
            {
                return CallNextHookEx(hHook, nCode, wParam, lParam);
            }
            MSLLHOOKSTRUCT mouseHook = (MSLLHOOKSTRUCT)Marshal.PtrToStructure(lParam, typeof(MSLLHOOKSTRUCT));
            RemoteConnection_client conn = focusController.getFocusedConnection();

            INPUT input = InputManager.getInputFromMouseHook(mouseHook, (Int32)wParam, ref toSend);
            if (toSend && conn.IsConnected)
            {
                //Logger.Clog("Sending mouse info");
                MouseConnection_client.SendMouseHook(input, conn.MouseEndPoint);
            }

            return 1;//CallNextHookEx(hHook, nCode, wParam, lParam);
        }

        public MouseHandler_client(RemoteClient client,ConnectionsManager fc)
        {
            this.client = client;
            focusController = fc;
            //KeyboardConnection_client.Connect(ipAddress);
            //destination = new IPEndPoint(IPAddress.Parse(ipAddress), 14000);
            MouseHookProcedure = new HookProc(MouseHandlerProcedure);
            /*
            hHook = SetWindowsHookEx(WH_MOUSE, MouseHookProcedure, (IntPtr)0, AppDomain.GetCurrentThreadId());
            if(hHook == 0)
            {
                MessageBox.Show("SetWindowsHookEx Failed");
                return;
            }
            */
        }

        public bool StartListeningHook()
        {
            if (hHook != 0) return false;
            hHook = SetWindowsHookEx(WH_MOUSE, MouseHookProcedure, (IntPtr)0, AppDomain.GetCurrentThreadId());
            if (hHook == 0) //MessageBox.Show("SetWindowsHookEx Failed");
                return false;
            return true;
        }

        public bool Stop()
        {
            if (hHook != 0 && !UnhookWindowsHookEx(hHook)) //MessageBox.Show("UnhookWindowsHookEx Failed");                
                return false;
            hHook = 0;
            return true;
        }
    }
}
