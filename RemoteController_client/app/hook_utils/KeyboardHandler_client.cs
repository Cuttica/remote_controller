﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RemoteController_client.app.hook_utils;
using static RemoteController_client.app.hook_utils.MouseHandler_client;
using RemoteController_server.model.implementations;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Diagnostics;
using RemoteController_server.Debug;

namespace RemoteController_client.app.hook_utils
{
    class KeyboardHandler_client
    {
        //public const uint FIRSTSCANCODE_CTRLALT = 2;
        //public const uint LASTSCANCODE_CTRLALT = 11;
        uint index;
        private bool ctrlIsPressed = false;
        private bool altIsPressed = false;

        HookProc KeyboardHookProcedure;

        //Declare the hook handle as an int.
        static int hHook = 0;

        private ConnectionsManager connectionManager;
        private RemoteClient app;
    
        public int KeyboardHandlerProcedure(int nCode, IntPtr wParam, IntPtr lParam)
        {
            if (nCode < 0 || !connectionManager.isThereAnyFocusedServer)
            {
                return CallNextHookEx(hHook, nCode, wParam, lParam);
            }

            KBDLLHOOKSTRUCT keyboardHook = (KBDLLHOOKSTRUCT)Marshal.PtrToStructure(lParam, typeof(KBDLLHOOKSTRUCT));

            var toSend = ElaborateHookStruct(keyboardHook);

            INPUT input = InputManager.getInputFromKeyboardHook(keyboardHook, (Int32)wParam);

            RemoteConnection_client conn = connectionManager.getFocusedConnection();
            if (toSend && conn.IsConnected)
            {
                conn.KeyboardConnection.SendMouseHook(input);
            }
            return 1;
        }

        private void SendReleaseModifiers(RemoteConnection_client conn)
        {
            foreach (INPUT input in InputManager.GetListOfReleasedModifierInput(altIsPressed, ctrlIsPressed))
            {
                conn.KeyboardConnection.SendMouseHook(input);
            }
        }

        public KeyboardHandler_client(RemoteClient app, ConnectionsManager fc)
        {
            this.app = app;
            connectionManager = fc;
            KeyboardHookProcedure = new HookProc(KeyboardHandlerProcedure);
           
        }
        public bool StartListeningHook()
        {
            if (hHook != 0) return false;
            hHook = SetWindowsHookEx(InputManager.WH_KEYBOARD_LL, KeyboardHookProcedure, (IntPtr)0, 0);
            if (hHook == 0) //MessageBox.Show("SetWindowsHookEx Failed");
                return false;
            return true;
        }
        /// <summary>
        /// Checks if the keyboard input is for the Client Application or Server Application: in the last case,
        /// return true and the input will be sent to the Server.
        /// https://msdn.microsoft.com/it-it/library/windows/desktop/dd375731
        /// </summary>
        /// <param name="keyboardHook"></param>
        /// <returns></returns>
        private bool ElaborateHookStruct(KBDLLHOOKSTRUCT keyboardHook)
        {

            RemoteConnection_client conn = connectionManager.getFocusedConnection();

            SetModifiers(keyboardHook);

            AltGrChecking(keyboardHook);
            //Console.WriteLine("ctrl: " + ctrlIsPressed + ", alt: " + altIsPressed + " (" +  keyboardHook.vkCode + " - " + keyboardHook.scanCode + " - " + keyboardHook.flags + ")");

            if (IsStopRequested(keyboardHook))
            {
                app.StopHookListening();
                if (conn.IsConnected)
                {
                    SendReleaseModifiers(conn);
                }
                return false;
            }

            if (IsSendRemoteRequest(keyboardHook))
            {
                if (conn.IsConnected) { 
                    app.SendRemotePaste();
                    SendReleaseModifiers(conn);
                }
                return false;
            }

            var toSend = true;
            switch (app.SwitchFocusCommand)
            {
                case KeyBindingsType.CTRLALT:
                    if (altIsPressed && ctrlIsPressed && 
                        keyboardHook.vkCode >= InputManager.FIRSTNUMBERCODE && keyboardHook.vkCode <= InputManager.LASTNUMBERCODE) //ALT + CTRL + N
                    {
                        Console.WriteLine("//ALT + CTRL + N");
                        //set focus 
                        index = (keyboardHook.vkCode - InputManager.FIRSTNUMBERCODE + 9) % 10;
                        if (conn.IsConnected)
                        {
                            SendReleaseModifiers(conn);
                        }
                        connectionManager.SetFocus((int)index);
                        toSend = false;
                    }
                    break;
                case KeyBindingsType.ALT:
                    if (altIsPressed && 
                        keyboardHook.vkCode >= InputManager.FIRSTNUMBERCODE && keyboardHook.vkCode <= InputManager.LASTNUMBERCODE) // ALT + N
                    {
                        Console.WriteLine("// ALT + N");
                        //set focus 
                        index = (keyboardHook.vkCode - InputManager.FIRSTNUMBERCODE + 9) % 10;
                        if (conn.IsConnected)
                        {
                            SendReleaseModifiers(conn);
                        }
                        connectionManager.SetFocus((int)index);
                        toSend = false;
                    }
                    break;
                case KeyBindingsType.CTRL:
                    if (ctrlIsPressed && 
                        keyboardHook.vkCode >= InputManager.FIRSTNUMBERCODE && keyboardHook.vkCode <= InputManager.LASTNUMBERCODE) //CTRL + N
                    {
                        Console.WriteLine("//CTRL + N");
                        //set focus 
                        index = (keyboardHook.vkCode - InputManager.FIRSTNUMBERCODE + 9) % 10;
                        if (conn.IsConnected)
                        {
                            SendReleaseModifiers(conn);
                        }
                        connectionManager.SetFocus((int)index);
                        toSend = false;
                    }
                    break;
            }
            return toSend;
        }

        private void SetModifiers(KBDLLHOOKSTRUCT keyboardHook)
        {
            if (keyboardHook.vkCode == InputManager.VK_CONTROL ||
                keyboardHook.vkCode == InputManager.VK_LCONTROL ||
                keyboardHook.vkCode == InputManager.VK_RCONTROL)
            {
                if (keyboardHook.flags == 0 || keyboardHook.flags == 1 || keyboardHook.flags == 32 || keyboardHook.flags == 33)
                {
                    ctrlIsPressed = true;
                }
                else if (keyboardHook.flags == 128 || keyboardHook.flags == 129 || keyboardHook.flags == 160)
                {
                    ctrlIsPressed = false;
                }

            }
            if (keyboardHook.vkCode == InputManager.VK_MENU ||
                keyboardHook.vkCode == InputManager.VK_LMENU ||
                keyboardHook.vkCode == InputManager.VK_RMENU)
            {
                if (keyboardHook.flags == 32 || keyboardHook.flags == 33 || keyboardHook.flags == 0 || keyboardHook.flags == 1)
                {
                    altIsPressed = true;
                }
                else if (keyboardHook.flags == 128 || keyboardHook.flags == 129)
                {
                    altIsPressed = false;
                }

            }
        }

        private bool IsStopRequested(KBDLLHOOKSTRUCT keyboardHook)
        {
            int stopModifierIndex = app.StopHookCommandModifier;
            int stopCommandCode = app.StopHookCommandIndex + InputManager.A_KEY;


            return (//se CTRL + STOPCOMMAND -> TOGLIERE L'HOOK
                (stopModifierIndex == 0 && ctrlIsPressed && keyboardHook.vkCode == stopCommandCode)
                //se ALT + STOPCOMMAND -> TOGLIERE L'HOOK
                || (stopModifierIndex == 1 && altIsPressed && keyboardHook.vkCode == stopCommandCode)
                //se ALT + CTRL + STOPCOMMAND -> TOGLIERE L'HOOK
                || (stopModifierIndex == 2 && altIsPressed && ctrlIsPressed && keyboardHook.vkCode == stopCommandCode)
                );           
        }

        private bool IsSendRemoteRequest(KBDLLHOOKSTRUCT keyboardHook)
        {
            int remotePasteModifier = app.RemotePasteCommandModifier;
            int remotePasteCommandCode = app.RemotePasteCommandIndex + InputManager.A_KEY;

            return (//se CTRL + REMOTEPASTECOMMAND -> INVIARE COMANDO
                (remotePasteModifier == 0 && ctrlIsPressed && keyboardHook.vkCode == remotePasteCommandCode)
                //se ALT + REMOTEPASTECOMMAND -> INVIARE COMANDO
                || (remotePasteModifier == 1 && altIsPressed && keyboardHook.vkCode == remotePasteCommandCode)
                //se ALT + CTRL + REMOTEPASTECOMMAND -> INVIARE COMANDO
                || (remotePasteModifier == 2 && altIsPressed && ctrlIsPressed && keyboardHook.vkCode == remotePasteCommandCode)
                );
        }

        public bool Stop()
        {
            if (hHook != 0 && !UnhookWindowsHookEx(hHook)) //MessageBox.Show("UnhookWindowsHookEx Failed");                
                return false;
            hHook = 0;
            return true;
        }

        private void AltGrChecking(KBDLLHOOKSTRUCT keyboardHook)
        {
            if(keyboardHook.vkCode == InputManager.VK_RMENU && (keyboardHook.flags == 32 || keyboardHook.flags == 33 || keyboardHook.flags == 0 || keyboardHook.flags == 1))
            {
                RemoteConnection_client conn = connectionManager.getFocusedConnection();
                if (conn.IsConnected)
                {
                    INPUT input = new INPUT();
                    input.Type = (int)InputTypes.INPUT_KEYBOARD;
                    input.Data.Keyboard.Vk = (ushort) 162;
                    input.Data.Keyboard.Scan = (ushort)541;
                    input.Data.Keyboard.Flags = (uint)KeyboardFlags.KEYEVENTF_KEYUP;
                    conn.KeyboardConnection.SendMouseHook(input);
                }
            }
        }
    }
}
