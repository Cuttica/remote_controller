﻿using RemoteController_server.Debug;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RemoteController_client.app
{
    delegate void GraphicChange_onDisconnect();
    delegate void GraphicalSetDownloadIconCallback(bool val);

    class RemoteConnection_client
    {
        private IPEndPoint mouseEndPoint;
        private KeyboardConnection_client keyboardConn;
        private ClipboardConnection clipboardConn;
        private bool isConnected;
        private Control connectionBox;
        private Control connectionsContainer;
        private ConnectionsManager connManager;
        private bool clipboardToSend;
        private Form1 form;

        public bool IsConnected { get { return isConnected; } set { isConnected = value; } }
        public Control ConnectionBox { get { return connectionBox; } set { connectionBox = value; } }
        public IPEndPoint MouseEndPoint { get { return mouseEndPoint; } set { mouseEndPoint = value; } }
        public KeyboardConnection_client KeyboardConnection { get { return keyboardConn; } set { keyboardConn = value; } }
        public ClipboardConnection ClipboardConnection { get { return clipboardConn; } }
        public ConnectionsManager ConnectionManager { get { return connManager; } }
        public bool ClipboardToSend { get { return clipboardToSend; } set { clipboardToSend = value; } }
        public Form1 App { get { return form; } }



        public RemoteConnection_client(Form1 form, ConnectionsManager manager, Control box, Control container)
        {
            this.form = form;
            isConnected = false;
            connectionBox = box;
            connManager = manager;
            connectionsContainer = container;
            connManager.AddConnection(this);
        }
        
        public void SwitchOnOff(object sender, EventArgs args)
        {
            if (isConnected)
            {
                keyboardConn.SendDisconnectionRequest();
                clipboardConn.SendDisconnectionRequest();
                Disconnect();
            }else
            {
                try
                {
                    Connect();
                }
                catch (SocketException)
                {
                    MessageBox.Show("IP address unreachable!");
                    Logger.Clog("IP address unreachable!");
                }
                catch (Exception) {
                    MessageBox.Show("Wrong IP address");
                    Logger.Clog("Wrong IP address");
                }
            }
        }

        public void Connect()
        {
            if (!isConnected)
            {
                form.SetLoadingCursor(true);
                string ipDest = connectionBox.Controls[0].Text;
                int port;
                try
                {
                    port = Int32.Parse(connectionBox.Controls[2].Text);
                }
                catch (Exception)
                {
                    MessageBox.Show("Invalid port number");
                    form.SetLoadingCursor(false);
                    return;
                }
                Logger.Clog(ipDest);
                try
                {
                    string password = ShowMyDialogBox();
                    connManager.App.Password = password;
                    clipboardConn = new ClipboardConnection(this, ipDest, port);
                    clipboardConn.ReceiveAsync();
                    clipboardConn.SendPassword(password);
                    form.SetLoadingCursor(false);
                }
                catch (Exception)
                {
                    MessageBox.Show("IP address port unreachable");
                    form.SetLoadingCursor(false);
                    return;
                }
                //keyboardConn = new KeyboardConnection_client(this, ipDest, 13000);
                //mouseEndPoint = new IPEndPoint(IPAddress.Parse(ipDest), 14000);

                //keyboardConn.ReceiveAsync();

                //isConnected = true;
                //connManager.SetFocus(this);
                //GraphicalConnect();
            }

        }

        public void Disconnect()
        {
            if (isConnected)
            {
                clipboardConn.Disconnect();
                keyboardConn.Disconnect();
                isConnected = false;
                connManager.App.RemoveFileChunks();
                GraphicalDisconnect();
            }
        }
        public void DeleteConnection(object o, EventArgs args)
        {
            var confirmResult = MessageBox.Show("Are you sure to remove this connection?",
                                     "Confirmation",
                                     MessageBoxButtons.YesNo);
            if (confirmResult != DialogResult.Yes)
            {
                return;
            }
            if (isConnected)
            {
                Disconnect();
            }
            connectionsContainer.Controls.Remove(connectionBox);
            connManager.RemoveConnection(this);
        }

        /// <summary>
        /// https://msdn.microsoft.com/en-us/library/ms171728.aspx
        /// </summary>
        public void GraphicalConnect()
        {
            FlowLayoutPanel panel = (FlowLayoutPanel)connectionBox;
            if (((TextBox)panel.Controls[0]).InvokeRequired)
            {
                GraphicChange_onDisconnect d = new GraphicChange_onDisconnect(GraphicalConnect);
                panel.Invoke(d, new object[] { });
            }
            else
            {
                ((TextBox)panel.Controls[0]).Enabled = false;
                ((PictureBox)panel.Controls[3]).BackColor = Color.Green;
                ((Button)panel.Controls[4]).Text = "Disconnect";
            }
        }

        /// <summary>
        /// https://msdn.microsoft.com/en-us/library/ms171728.aspx
        /// </summary>
        public void GraphicalDisconnect()
        {
            Console.WriteLine("clipboard -> " + clipboardConn.IsConnected);
            Console.WriteLine("key -> " + keyboardConn.IsConnected);
            Console.WriteLine(""+clipboardConn.IsConnected);
            FlowLayoutPanel panel = (FlowLayoutPanel)connectionBox;
            if (((TextBox)panel.Controls[0]).InvokeRequired)
            {
                GraphicChange_onDisconnect d = new GraphicChange_onDisconnect(GraphicalDisconnect);
                panel.Invoke(d, new object[] { });
            }
            else
            {
                ((TextBox)panel.Controls[0]).Enabled = true;
                ((PictureBox)panel.Controls[3]).BackColor = Color.Red;
                ((Button)panel.Controls[4]).Text = "Connect";
            }
        }

        public void GraphicalSetDownloadIcon(bool val)
        {
            FlowLayoutPanel panel = (FlowLayoutPanel)connectionBox;
            if (((TextBox)panel.Controls[0]).InvokeRequired)
            {
                GraphicalSetDownloadIconCallback d = new GraphicalSetDownloadIconCallback(GraphicalSetDownloadIcon);
                panel.Invoke(d, new object[] { val });
            }
            else
            {
                if (val)
                {
                    ((PictureBox)panel.Controls[5]).Image = Image.FromFile(".\\..\\..\\img\\clipboard20.png");
                    ((PictureBox)panel.Controls[5]).BackColor = Color.PaleGreen;
                }else
                {
                    ((PictureBox)panel.Controls[5]).Image = null;
                    ((PictureBox)panel.Controls[5]).BackColor = Color.LightGray;
                }
            }
        }

        public string ShowMyDialogBox()
        {
            PasswordInput testDialog = new PasswordInput();

            // Show testDialog as a modal dialog and determine if DialogResult = OK.
            string password;
            if (testDialog.ShowDialog(form) == DialogResult.OK)
            {
                // Read the contents of testDialog's TextBox.
                password = testDialog.Password;
            }
            else
            {
                password = "";
            }
            testDialog.Dispose();
            return password;
        }   



    }
}
