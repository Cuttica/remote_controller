﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Windows.Input;
using System.Drawing;
using RemoteController_server.model.implementations;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.Security;
using RemoteController_server.Debug;
using System.IO;
using System.Windows.Forms;
using RemoteController_client.app;

namespace RemoteController_client
{
    class KeyboardConnection_client
    {
        private TcpClient client;
        private NetworkStream stream;
        private bool isConnected = false;
        private IFormatter formatter;
        private RemoteConnection_client conn;

        public bool IsConnected { get { return isConnected; } }

        public KeyboardConnection_client(RemoteConnection_client conn, String server, Int32 port)
        {
            this.conn = conn;
            {
                try
                {
                    client = new TcpClient(server, port);
                    isConnected = true;
                    stream = client.GetStream();
                    formatter = new BinaryFormatter();
                    Logger.Clog("Keyboard Connesso");
                    
                }
                catch (SocketException e)
                {
                    Logger.Clog("Keyboard Indirizzo IP non raggiungibile");
                    throw e;
                }
            }
        }
        public void SendMouseHook(INPUT packet)
        {
            //todo se errore serializzazione chiudere il canale!
            try
            {
                if (client.Connected)
                     formatter.Serialize(stream, packet);
            }
            catch (IOException)
            {
                Logger.wr("Server has closed the connection.");
                //Disconnect();
                //conn.GraphicalDisconnect();
                conn.Disconnect();
            }
            catch (SerializationException)
            {
                Logger.wr("Server has closed the connection.");
                conn.Disconnect();
            }
            catch (Exception)
            {
                Logger.wr("General Exception");
                conn.Disconnect();
            }
        }

        public Task ReceiveAsync()
        {
             return Task.Run(() => {
                try
                {
                     INPUT input = (INPUT)formatter.Deserialize(stream);
                     if(input.Type == (int)InputTypes.INPUT_STOP)
                     {
                         Logger.Clog("KEYBOARD: Ricevuta richiesta di chiudere connessione dal server!");
                         this.Disconnect();
                         conn.GraphicalDisconnect();
                     }
                     return;
                }
                catch (IOException)
                {
                     //Se opero su connessione chiusa
                    Logger.Clog("Connection Closed");
                     //this.Disconnect();
                     //conn.GraphicalDisconnect();
                     conn.Disconnect();
                     return;
                }
                catch (SerializationException)
                {
                    Logger.Clog("Serialization Error");
                     conn.Disconnect();
                     return;

                 }
                 catch (Exception)
                 {
                     Logger.Clog("Connection Closed");
                     conn.Disconnect();
                     return;
                 }
            });
        }

        public void SendDisconnectionRequest()
        {
            if (stream != null && client.Connected)
            {
                INPUT req = new INPUT();
                req.Type = (int)InputTypes.INPUT_STOP;
                formatter.Serialize(stream, req);
            }
            
        }

        public void Disconnect()
        {
            // Close everything.
            //dico al server che voglio chiudere
            stream.Close();
            client.Close();
            isConnected = false;
            conn.IsConnected = false;
            Logger.Clog("Disconnesso");
        }

    }
}
