﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Windows.Input;
using System.Drawing;
using RemoteController_server.model.implementations;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.Security;
using RemoteController_server.Debug;
using System.IO;
using System.Windows.Forms;
using System.Collections;
using System.Runtime.InteropServices;
using System.Threading;
using System.Collections.Concurrent;

namespace RemoteController_client
{
    class AsyncKeyboardConnection_client
    {
        private TcpClient client;
        private NetworkStream stream;
        private Boolean _isConnected = false;
        private IFormatter formatter;
        BlockingCollection<INPUT> dataItems = new BlockingCollection<INPUT>(100);

        private byte[] buffer;
        private int sizeOfInput;
        private int bytesToSend;
        private int bytesSended;

        private ManualResetEvent toConsumeExists = new ManualResetEvent(false);
        private ManualResetEvent toProduceExists = new ManualResetEvent(false);

        public AsyncKeyboardConnection_client(String server, Int32 port)
        {
                try
                {
                    client = new TcpClient();

                    sizeOfInput = Marshal.SizeOf(new INPUT());
                    bytesToSend = sizeOfInput;
                    bytesSended = 0;
                    buffer = new byte[bytesToSend];

                    client.BeginConnect(server, port,
                        new AsyncCallback(ConnectCallback), client);
                }
                catch (ArgumentNullException e)
                {
                    Console.WriteLine("ArgumentNullException: {0}", e);
                    throw e;
                }
                catch (SocketException e)
                {
                    MessageBox.Show("indirizzo IP non raggiungibile");
                    throw e;
                }
        }

        private void ConnectCallback(IAsyncResult ar)
        {
            TcpClient client = (TcpClient)ar.AsyncState;

            // Complete the connection.
            client.EndConnect(ar);

            Console.WriteLine("Socket connected to {0}",
                client.Client.RemoteEndPoint.ToString());

            SendMouseHookAsync();


        }

        public void SendMouseHookAsync()
        {
            while (!dataItems.IsCompleted)
            {
                INPUT data;
                // Blocks if number.Count == 0
                // IOE means that Take() was called on a completed collection.
                // Some other thread can call CompleteAdding after we pass the
                // IsCompleted check but before we call Take. 
                // In this example, we can simply catch the exception since the 
                // loop will break on the next iteration.
                try
                {
                    data = dataItems.Take();
                    Console.WriteLine("invio -> Vk: {0}, Type: {1}", data.Data.Keyboard.Vk, data.Type);
                    InputManager.getBytesFromInput(data, ref bytesToSend);
                    Console.WriteLine(bytesToSend);
                    client.GetStream().BeginWrite(buffer, 0, bytesToSend, new AsyncCallback(SendCallback), client.GetStream());
                }
                catch (InvalidOperationException) { }
            }
        }

        public void SendCallback(IAsyncResult ar)
        {
            NetworkStream stream = (NetworkStream)ar.AsyncState;
            int bytesSent = stream.EndRead(ar);

            Console.WriteLine("Sent {0} bytes to server.", bytesSent);

        }

        public void SendMouseHook(INPUT packet)
        {
            dataItems.Add(packet);
        }
        
        public void Close()
        {

            // Let consumer know we are done.
            dataItems.CompleteAdding();
            // Close everything.
            stream.Close();
            client.Close();
            _isConnected = false;
            MessageBox.Show("Disconnesso");
        }

    }
}

