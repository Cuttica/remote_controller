﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RemoteController_client.app;
using System.Windows.Input;
using RemoteController_client.app.hook_utils;
using RemoteController_server.Debug;
using System.Net;
using System.IO;

namespace RemoteController_client
{
    public delegate void SetFocusCallback(int index);
    public delegate void SetClipboardCallback(string format, object data);
    public delegate void SetLoadingCursorCallback(bool loading);

    public partial class Form1 : Form
    {
        private RemoteClient app;

        private const int CONNECTIONBLOCK_WIDTH = 310;
        private const int CONNECTIONBLOCK_HEIGHT = 50;
        private const int CONNECTIONBLOCK_COMPONENT_HEIGHT = 20;
        private const int CONNECTIONBLOCK_COMPONENT_PAD = (CONNECTIONBLOCK_HEIGHT - CONNECTIONBLOCK_COMPONENT_HEIGHT) / 2;

        public Form1()
        {
            InitializeComponent();
            app = new RemoteClient(this);
            flowLayoutPanel1.Controls.Add(addRemoteConnectionBlock("192.168.1.103"));
            flowLayoutPanel1.Controls.Add(addRemoteConnectionBlock("192.168.1.104"));
            flowLayoutPanel1.Controls.Add(addRemoteConnectionBlock("192.168.43.51"));
            flowLayoutPanel1.Controls.Add(addRemoteConnectionBlock("127.0.0.1"));

            //Add Default Setting options
            comboBox1.SelectedIndex = 0;
            comboBox4.SelectedIndex = 1;
            comboBox2.SelectedIndex = 16;
            comboBox3.SelectedIndex = 0;
            comboBox5.SelectedIndex = 5;


        }

        private void button1_Click(object sender, EventArgs e)
        {
            IPEndPoint ip1 = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 0);
            IPEndPoint ip2 = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 0);
            Console.WriteLine("ip1 == ip2: " + (ip1 == ip2));
            Console.WriteLine("ip1 equals ip2: " + (ip1.Equals(ip2)));
            Console.WriteLine("add1 == add2: " + (ip1.Address == ip2.Address));
            Console.WriteLine("add1 equals add2: " + (ip1.Address.Equals(ip2.Address)));
        }


        private void button3_Click(object sender, EventArgs e)
        {
            if (app.ConnectionsManager.isThereAnyFocusedServer)
            {
                if (!app.StartHookListening())
                {
                    MessageBox.Show("SetWindowsHookEx Failed");
                    return;
                }
                FullScreen(true);
            }
        }
        /// <summary>
        /// Set fullscreen if fs == true
        /// </summary>
        /// <param name="fs"></param>
        public void FullScreen(bool fs)
        {
            if (fs)
            {
                button3.Enabled = false;
                button5.Enabled = false;
                FormBorderStyle = FormBorderStyle.None;
                WindowState = FormWindowState.Maximized;
            }else
            {
                button3.Enabled = true;
                button5.Enabled = true;
                FormBorderStyle = FormBorderStyle.FixedDialog;
                WindowState = FormWindowState.Normal;
            }
        }

        internal void SetLoadingCursor(bool loading)
        {
            if (flowLayoutPanel1.InvokeRequired)
            {
                SetLoadingCursorCallback d = new SetLoadingCursorCallback(SetLoadingCursor);
                this.Invoke(d, new object[] { loading });
            }
            else
            {
                if (loading)
                {
                    this.Cursor = Cursors.WaitCursor;
                }else
                {
                    this.Cursor = Cursors.Arrow;
                }
            }
        }

        /// <summary>
        /// Add RemoteConnection control block.
        /// </summary>
        private void button5_Click(object sender, EventArgs e)
        {
            if (flowLayoutPanel1.Controls.Count != ConnectionsManager.MAXCONNECTIONS)
            {
                flowLayoutPanel1.Controls.Add(addRemoteConnectionBlock("127.0.0.1"));
            }
        }

        private FlowLayoutPanel addRemoteConnectionBlock(string addr)
        {
            TextBox addrArea = new TextBox();
            addrArea.Size = new Size(70, CONNECTIONBLOCK_COMPONENT_HEIGHT);
            addrArea.Text = addr;
            addrArea.Margin = new Padding(10, CONNECTIONBLOCK_COMPONENT_PAD, 3, CONNECTIONBLOCK_COMPONENT_PAD);
            addrArea.TabStop = false;

            Label portLaber = new Label();
            portLaber.Size = new Size(8, CONNECTIONBLOCK_COMPONENT_HEIGHT);
            portLaber.Text = ":";
            portLaber.Margin = new Padding(0, CONNECTIONBLOCK_COMPONENT_PAD, 0, CONNECTIONBLOCK_COMPONENT_PAD);

            TextBox portArea = new TextBox();
            portArea.Size = new Size(50, CONNECTIONBLOCK_COMPONENT_HEIGHT);
            portArea.Margin = new Padding(3, CONNECTIONBLOCK_COMPONENT_PAD, 3, CONNECTIONBLOCK_COMPONENT_PAD);
            portArea.TabStop = false;

            PictureBox connectPic = new PictureBox();
            connectPic.BackColor = Color.Red;
            connectPic.Size = new Size(12, CONNECTIONBLOCK_COMPONENT_HEIGHT);
            connectPic.Margin = new Padding(3, CONNECTIONBLOCK_COMPONENT_PAD, 3, CONNECTIONBLOCK_COMPONENT_PAD);

            Button connectBtn = new Button();
            connectBtn.Size = new Size(54, CONNECTIONBLOCK_COMPONENT_HEIGHT);
            connectBtn.Text = "connect";
            connectBtn.Margin = new Padding(3, CONNECTIONBLOCK_COMPONENT_PAD, 3, CONNECTIONBLOCK_COMPONENT_PAD);
            connectBtn.BackColor = Color.White;
            connectBtn.TabStop = false;

            PictureBox downloadPic = new PictureBox();
            downloadPic.Size = new Size(20, CONNECTIONBLOCK_COMPONENT_HEIGHT);
            downloadPic.Margin = new Padding(9, CONNECTIONBLOCK_COMPONENT_PAD, 9, CONNECTIONBLOCK_COMPONENT_PAD);
            downloadPic.BackColor = Color.LightGray;

            Button deleteBtn = new Button();
            deleteBtn.Size = new Size(26, CONNECTIONBLOCK_COMPONENT_HEIGHT);
            deleteBtn.Margin = new Padding(3, CONNECTIONBLOCK_COMPONENT_PAD, 10, CONNECTIONBLOCK_COMPONENT_PAD);
            deleteBtn.Image = Image.FromFile(".\\..\\..\\img\\remove12.png");
            deleteBtn.BackColor = Color.White;
            deleteBtn.TabStop = false;

            FlowLayoutPanel container = new FlowLayoutPanel();
            container.BorderStyle = BorderStyle.FixedSingle;
            container.Size = new Size(CONNECTIONBLOCK_WIDTH, CONNECTIONBLOCK_HEIGHT);
            container.Controls.AddRange(new Control[] {addrArea, portLaber, portArea, connectPic, connectBtn, downloadPic, deleteBtn });


            RemoteConnection_client conn = new RemoteConnection_client(this, app.ConnectionsManager,container, flowLayoutPanel1);
          
            connectBtn.Click += conn.SwitchOnOff;
            deleteBtn.Click += conn.DeleteConnection;

            return container;
        }

        /// <summary>
        /// Settings Options Changed
        /// </summary>
        /// <param name="o"></param>
        /// <param name="args"></param>
        public void SwitchFocusCommands_onChange(object o, EventArgs args)
        {
            app.SwitchFocusCommand = (KeyBindingsType)comboBox1.SelectedIndex;
        }
        public void StopHookCommandModifier_onChange(object o, EventArgs args)
        {
            ComboBox c = o as ComboBox;
            app.StopHookCommandModifier = c.SelectedIndex;
        }
        public void StopHookCommandLetter_onChange(object o, EventArgs args)
        {
            ComboBox c = o as ComboBox;
            app.StopHookCommandIndex = c.SelectedIndex;
        }
        public void RemotePasteCommandModifier_onChange(object o, EventArgs args)
        {
            ComboBox c = o as ComboBox;
            app.RemotePasteCommandModifier = c.SelectedIndex;
        }
        public void RemotePasteCommandLetter_onChange(object o, EventArgs args)
        {
            ComboBox c = o as ComboBox;
            app.RemotePasteCommandIndex = c.SelectedIndex;
        }



        public void SetFocus(int index)
        {
            if (flowLayoutPanel1.InvokeRequired)
            {
                SetFocusCallback d = new SetFocusCallback(SetFocus);
                this.Invoke(d, new object[] { index });
            }
            else
            {
                foreach(Control c in flowLayoutPanel1.Controls)
                {
                    ((FlowLayoutPanel)c).BackColor = Color.Transparent;
                }
                ((FlowLayoutPanel)flowLayoutPanel1.Controls[index]).BackColor = Color.LightGreen;
            }
        }

        public void SetClipboardData(string format, object data)
        {
            if (flowLayoutPanel1.InvokeRequired)
            {
                SetClipboardCallback d = new SetClipboardCallback(SetClipboardData);
                this.Invoke(d, new object[] { format, data });
            }
            else
            {
                Clipboard.SetData(format, data);
            }
        }
    }
}
