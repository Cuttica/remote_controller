﻿using RemoteController_server.model.implementations.hook_utils;
using RemoteController_server.model.implementations.marshalling_utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;

namespace RemoteController_server.model.implementations
{
    class RemoteConnection_server
    { 
        private TcpServerConnection keyboardConn;
        private UdpServerConnection mouseConn;
        private AsyncClipboardConnection clipboardConn;
        private RemoteServer app;

        public ServerConnectionStatus Status { get { return keyboardConn.Status; } }
        public TcpServerConnection KeyboardConnection { get { return keyboardConn; } set { keyboardConn = value; } }
        public AsyncClipboardConnection ClipboardConnection { get { return clipboardConn; } set { clipboardConn = value; } }
        public UdpServerConnection MouseConnection { get { return mouseConn; } set { mouseConn = value; } }
        public RemoteServer App { get { return app; } }

        public RemoteConnection_server(RemoteServer app, IPAddress address, int port)
        {
            this.app = app;

            mouseConn = new AsyncUdpServerConnection(this, 0);
            clipboardConn = new AsyncClipboardConnection(this, address, 0);
            keyboardConn = new AsyncKeyboardConnection(this, address, 0); //Versione con _Async & await

            mouseConn.onReceive += MouseKeyboardHookHandler_server.MouseHandler_onReceiveUDP;       
            keyboardConn.onReceive += MouseKeyboardHookHandler_server.KeyboardHandler_onReceiveTCPAsync;
            clipboardConn.onReceive += ClipboardAndConfigurationDataManager.ConfigurationHandler_onReceive;
        }

        public void Start()
        {
            keyboardConn?.Run();
            if (mouseConn != null && !mouseConn.IsReceiving)
            {
                mouseConn.Run();
            }
            clipboardConn?.Run();
        }

        public void Stop()
        {
            mouseConn?.CloseConnection();
            keyboardConn?.CloseConnection();
            clipboardConn?.CloseConnection();
        }

        //public void CloseClientConnection()
        //{
        //    if (this.Status == ServerConnectionStatus.Connected)
        //    {
        //        keyboardConn.CloseClientConnection();
        //        mouseConn.CloseClientConnection();
        //        clipboardConn.CloseClientConnection();
        //    }
        //}
    }
}
