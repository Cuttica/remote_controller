﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.IO;
using System.Runtime.InteropServices;
using RemoteController_server.model.implementations;
using RemoteController_server.Debug;

namespace RemoteController_server.model.implementations.hook_utils
{

    
    class MouseKeyboardHookHandler_server
    {
        const int CXSCREEN = 0;
        const int CYSCREEN = 1;

        private static BinaryFormatter formatter;
        private static int screenWidth = GetSystemMetrics(CXSCREEN);
        private static int screenHeight = GetSystemMetrics(CYSCREEN);

        public static int ScreenWidth { get { return screenWidth; } set { screenWidth = value; } }
        public static int ScreenHeight { get { return screenHeight; } set { screenHeight = value; } }

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern int GetSystemMetrics(int nIndex);

        [DllImport("user32.dll", SetLastError = true)]
        private static extern uint SendInput(uint numberOfInputs, INPUT[] inputs, int sizeOfInputStructure);

        public static void MouseHandler_onReceiveUDP(object o, EventArgs args)
        {
            INPUT inputPacket = InputManager.getInputFromBytes(((ByteBufferStream)args).Data);
            if (SendInput(1, new INPUT[] { inputPacket }, Marshal.SizeOf(typeof(INPUT))) == 0) throw new InvalidOperationException();
            
        }

        
        public static void KeyboardHandler_onReceiveTCP(object o, EventArgs args)
        {
            TcpServerConnection server = (TcpServerConnection)o;
            //Console.WriteLine("ho ricevuto un pacchetto");
            INPUT inputPacket = InputManager.getInputFromBytes(((ByteBufferStream)args).Data);
            //Console.WriteLine("type: " + inputPacket.Type);
            //Console.WriteLine("data: " + inputPacket.Data.Keyboard.Vk);
            if (SendInput(1, new INPUT[] { inputPacket }, Marshal.SizeOf(typeof(INPUT))) == 0) throw new Exception();

        }

        public static void KeyboardHandler_onReceiveTCPAsync(object o, EventArgs args)
        {
            INPUT inputPacket = ((ByteBufferStream)args).Input;
            AsyncKeyboardConnection conn = o as AsyncKeyboardConnection;
            if (inputPacket.Type == (uint)InputTypes.INPUT_STOP)
            {
                conn.CloseClientConnection();
                return;
            }
            //Logger.Slog("Ho ricevuto un INPUT -> ");
            //Console.WriteLine("type: " + inputPacket.Type);
            //Console.WriteLine("data: " + inputPacket.Data.Keyboard.Vk);
            if (SendInput(1, new INPUT[] { inputPacket }, Marshal.SizeOf(typeof(INPUT))) == 0) throw new Exception();
        }

        /*
        public static void KeyboardHandler_onReceiveTCPFormatter(object o, EventArgs args)
        {
            if (formatter == null) formatter = new BinaryFormatter();
            TcpServerConnection server = (TcpServerConnection)o; 
            try
            {
                var inputPacket = (INPUT)formatter.Deserialize(((ByteBufferStream)args).Stream);
                Console.WriteLine("ho ricevuto un pacchetto");
                //Console.WriteLine(InputManager.getInfosFromInput(inputPacket));
                if(SendInput(1, new INPUT[] { inputPacket }, Marshal.SizeOf(typeof(INPUT))) == 0) throw new Exception();
            }
            catch (SerializationException e)
            {
                Console.WriteLine("Failed to serialize. Reason: " + e.Message);
                server.CloseClientConnection();
            }
            catch (System.IO.IOException e2)
            {
                Logger.Slog("Client close connection.");
                server.CloseClientConnection();
            }
            
        }
        */

    }
}
