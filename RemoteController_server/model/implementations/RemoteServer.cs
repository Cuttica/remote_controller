﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using RemoteController_server.model.implementations.hook_utils;
using RemoteController_server.model.implementations.clipboard_utils;
using System.Windows.Forms;
using System.IO;

namespace RemoteController_server.model.implementations
{
    /// <summary>
    /// TODO: new feature -> advertisement and sollecitation or https://msdn.microsoft.com/en-us/library/aa365920(VS.85).aspx 
    /// for get the ipaddress connected through internet.
    /// </summary>
    class RemoteServer
    {
        private string localStoragePath;

        private RemoteConnection_server connection;
        private ClipboardManager clipboardManager;
        private RemoteController_server form;
        private string password;

        public ClipboardManager ClipboardManager { get { return clipboardManager; } }
        public RemoteConnection_server Connection { get { return connection; } set { connection = value; } }
        public RemoteController_server Form { get { return form; } }
        public string LocalStoragePath { get { return localStoragePath; } }
        public string Password { get { return password; } set { password = value; } }
        

        public RemoteServer(RemoteController_server form)
        {
            this.form = form;
            localStoragePath = Path.GetFullPath(".\\..\\..\\localStorage\\");

            connection = new RemoteConnection_server(this, IPAddress.Any, 0);
            clipboardManager = new ClipboardManager(form.Handle, connection);
        }

        public void Start()
        {
            if (System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
            {
                connection.Start();
                clipboardManager.Start();
            }else
            {
                MessageBox.Show("no internet avaiable");
            }
        }

        public void Stop()
        {
            connection.Stop();
            clipboardManager.Stop();
            this.form.ShowClipboardAddress(-1);
        }

        public List<string> GetLocalIPAddresses()
        {
            List<string> list = new List<string>();

            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    list.Add(ip.ToString());
                }
            }
            return list;
        }
    }
}
