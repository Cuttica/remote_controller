﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Windows.Forms;
using RemoteController_server.Debug;


namespace RemoteController_server.model.implementations
{
    class UdpServerConnection
    {
        protected RemoteConnection_server remoteConnection;
        public event DataHandler onReceive;
        protected UdpClient server;
        protected IPEndPoint endpoint;
        protected volatile bool stopReceveing;
        protected bool isReceiving;
        protected Int32 port;

        public Int32 Port { get { return port; } }
        public RemoteConnection_server RemoteConnection { get { return remoteConnection; } }
        public bool IsReceiving { get { return isReceiving; } }
        public bool Stop { get { return stopReceveing; } set { stopReceveing = value; } }

        public UdpServerConnection(RemoteConnection_server rc, Int32 port)
        {
            isReceiving = false;
            remoteConnection = rc;
            //server.Client.NoDelay = true;
        }


        public virtual void CloseConnection()
        {           
            if (server != null)  server.Close();
        }
        protected virtual void Init()
        {
            server = new UdpClient(port);
            //server.AllowNatTraversal(true);
            endpoint = ((IPEndPoint)server.Client.LocalEndPoint);
            this.port = endpoint.Port;
            Logger.Slog("UDP waiting packets on port: " + this.port + ", addr: " + ((IPEndPoint)server.Client.LocalEndPoint).Address);
        }

        public virtual void Run()
        {
            Init();
            while (!stopReceveing)
            {
                try
                {
                    Byte[] receiveBytes = server.Receive(ref endpoint);
                    //TODO: accettare il pacchetto solo è arrivato dal client! non accettare qualsiasi pacchetto udp che mi arriva!
                    onReceive(this, new ByteBufferStream(receiveBytes));
                }catch (SocketException) {
                    Console.WriteLine("Server close connection.");
                }
            }
        }

        protected virtual void OnReceive(EventArgs e)
        {
            onReceive?.Invoke(this, e);
        }

        public virtual void CloseClientConnection()
        {
            stopReceveing = true;
        }
    }
}
