﻿using RemoteController_server.Debug;
using RemoteController_server.model.implementations.marshalling_utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace RemoteController_server.model.implementations
{
    class AsyncKeyboardConnection : AsyncTcpServerConnection
    {
        public AsyncKeyboardConnection(RemoteConnection_server remoteConnection, IPAddress addr, int port) : base(remoteConnection, addr, port)
        {
        }

        protected async override Task ElaborateIncomingStream()
        {
            while (true)
            {
                INPUT input = await InputSerialization.DeserializeAsync(this, client.GetStream());

                if (base.serverStop)
                { 
                    CloseClientConnection();
                    server.Stop();
                    status = ServerConnectionStatus.Disconnected;
                    remoteConnection.App.Stop();
                    return;
                }
                if (base.clientStop)
                {
                    CloseClientConnection();
                    break;
                }
                OnReceive(new ByteBufferStream(input));
            }
        }

        public Task SendStopMessage()
        {
            INPUT msg = new INPUT();
            msg.Type = (uint) InputTypes.INPUT_STOP;
            return InputSerialization.SerializeAsync(msg, client.GetStream());
        }

        public async override void CloseConnection()
        {
            if (base.status == ServerConnectionStatus.Connected)
            {
                base.serverStop = true;
                if (client.Connected)
                {
                    await SendStopMessage();
                }
                // Shutdown and end connection
                CloseClientConnection();
            }
            if (base.status == ServerConnectionStatus.Listening)
            {
                stopServerCancelSource.Cancel();
            }
            base.status = ServerConnectionStatus.Disconnected;
        }

        public override void CloseClientConnection()
        {
            if (base.status == ServerConnectionStatus.Connected && client != null)
            {
                //in qualche modo arrivo a fare questa operazione anche quando il socket è già stato chiuso.
                if (client.Connected)
                {
                    client.GetStream().Close(); //da testare che eccezione genera
                }
                client.Close();
                client = null;
            }
        }
    }
}
