﻿using RemoteController_server.Debug;
using RemoteController_server.model.implementations.marshalling_utils;
using RemoteController_server.model.interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace RemoteController_server.model.implementations
{
    class AsyncClipboardConnection : AsyncTcpServerConnection, IClipboardSender
    {
        public AsyncClipboardConnection(RemoteConnection_server remoteConnection, IPAddress addr, int port) : base(remoteConnection, addr, port)
        {
        }

        /// <summary>
        /// Needed for showing to user the actual listening addresses
        /// </summary>
        public override void StartListening()
        {
            base.StartListening();

            this.remoteConnection.App.Form.ShowClipboardAddress(this.port);
        }

        /// <summary>
        /// First of all, sends a configuration message to just-connected client.
        /// </summary>
        /// <returns></returns>
        protected override async Task ElaborateIncomingStream()
        {
            ///PASSWORD
            AppData confData = await ClipboardAndConfigurationDataManager.DeserializeAsync(this, client.GetStream());
            if (base.serverStop)
            {
                Logger.Slog("STOPPING SERVER.");
                CloseClientConnection();
                server.Stop();
                status = ServerConnectionStatus.Disconnected;
                remoteConnection.App.Stop();
                return;
            }
            if (base.clientStop || confData.Type != DataType.Configuration)
            {
                CloseClientConnection();
                return;
            }
            if(confData.Content.Clipboard.Password.CompareTo(
                remoteConnection.App.Password) != 0)
            {
                CloseClientConnection();
                return;
            }

            this.remoteConnection.App.Form.SetVisibilityNotifyIcon(false);
            await SendMessage(DataType.Configuration);
            while (true)
            {
                AppData receivedData = await ClipboardAndConfigurationDataManager.DeserializeAsync(this, client.GetStream());//.WithWaitCancellation();

                if (base.serverStop)
                {
                    Logger.Slog("STOPPING SERVER.");
                    CloseClientConnection();
                    server.Stop();
                    status = ServerConnectionStatus.Disconnected;
                    remoteConnection.App.Stop();
                    return;
                }
                if (base.clientStop)
                {
                    CloseClientConnection();
                    break;
                }
                OnReceive(new ByteBufferStream(receivedData));
            }
        }
        
        public Task<bool> SendMessage(DataType type)
        {
            AppData msg = new AppData();
            msg.Type = type;
            if(type == DataType.Configuration)
            {
                msg.Content.Configuration = new ConfigurationData();
                msg.Content.Configuration.keyboardConnPort = base.remoteConnection.KeyboardConnection.Port;
                msg.Content.Configuration.mouseConnPort = base.remoteConnection.MouseConnection.Port;
            }
            return ClipboardAndConfigurationDataManager.SerializeAsync(msg, client.GetStream());
        }

        public void SendClipboardData(AppData data)
        {
            if (client.Connected)
            {
                ClipboardAndConfigurationDataManager.Serialize(data, client.GetStream());
            }
        }

        public override async void CloseConnection()
        {
            if (base.status == ServerConnectionStatus.Connected)
            {
                base.serverStop = true;
                if (client.Connected)
                {
                    await SendMessage(DataType.Stop);
                }
                // Shutdown and end connection
                CloseClientConnection();
            }
            if (base.status == ServerConnectionStatus.Listening)
            {
                stopServerCancelSource.Cancel();
            }
            base.status = ServerConnectionStatus.Disconnected;
        }

        public override void CloseClientConnection()
        {
            if (base.status == ServerConnectionStatus.Connected && client != null)
            {
                if (client.Connected)
                {
                    client.GetStream().Close();
                }
                client.Close();
                client = null;
            }
        }
    }
}
