﻿using RemoteController_server.Debug;
using RemoteController_server.model.implementations.clipboard_utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RemoteController_server.model.implementations.marshalling_utils
{
    class InputSerialization
    {
        private static BinaryFormatter formatter = new BinaryFormatter();

        public static Task<INPUT> DeserializeAsync(AsyncKeyboardConnection conn, Stream stream)
        {
            return Task.Run(() => {
                try
                {
                    INPUT input;
                    input = (INPUT)formatter.Deserialize(stream);
                    if(input.Type == (int)InputTypes.INPUT_STOP)
                    {
                        Logger.Slog("Received stop request by client input");
                        conn.ClientStop = true;
                    }
                    return input;
                }
                catch (IOException)
                {
                    ///I enter here when 1) father close connection; 2)Connection lost; in any case set stopServer flag and exit
                    Logger.Slog("Connection lost.");
                    conn.ServerStop = true;
                    return new INPUT();
                }
                catch (SerializationException)
                {
                    ///pacchetto corrotto o mi hanno inviato porcheria o client e server hanno differenti versioni della 
                    ///struttura INPUT.
                    ///NON so ancora bene come gestirlo: se si può scartare semplicemente il pacchetto o occorre chiudere lo stream
                    ///per ora chiudo la connessione per precauzione.
                    Logger.Slog("Connection lost. Stopping the Server.");
                    conn.ServerStop = true;
                    return new INPUT();

                }
                catch (Exception e)
                {
                    //Altre eccezioni non desiderate, stoppo tutto lo stesso!
                    Logger.Slog("General exception on Deserialize Input (InputSerialization:53); Message:\n" + e.Message);
                    conn.ServerStop = true;
                    return new INPUT();
                }
            });
        }
        public static Task<bool> SerializeAsync(INPUT input, Stream stream)
        {
            return Task.Run(() => {
                try
                {
                    //Logger.Slog("Invio messaggio");
                    formatter.Serialize(stream, input);
                    return true;
                }
                catch (IOException e1)
                {
                    ///Entro qua quando perdo connettività proprio quando sto inviando il messaggio di stop.
                    ///Non posso più effettuare l'operazione, quindi esco semplicemente, poichè ottengo lo 
                    ///stesso risultato quando il client sentirà la perdita di connessione ->chiuderà la connesione
                    Logger.Slog("Connection Lost.");
                    return false;
                }
                catch (SerializationException e)
                {
                    ///Per qualche ragione non riesco a serializzare l'input, probabilemnte client e server hanno diverse
                    ///versioni della struttura INPUT (2 compilazioni diverse). Anche in questo caso il client se ne accorgerà
                    ///quando gli chiuderò in faccia la connessione.
                    Logger.Slog("Fail to Send message.");
                    return false;
                }catch(Exception e)
                {
                    ///Qualsiasi altra turba non mando nulla.
                    Logger.Slog("Fail to Send message.");
                    return false;
                }
            });
        }
    }
}
