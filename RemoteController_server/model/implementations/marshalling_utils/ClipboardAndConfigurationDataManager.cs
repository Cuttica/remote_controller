﻿using RemoteController_server.Debug;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Threading;
using System.Collections.Specialized;
using RemoteController_server.model.implementations.clipboard_utils;

namespace RemoteController_server.model.implementations.marshalling_utils
{
    public enum DataType
    {
        UnicodeText, ASCIIText, File, Music, Directory, Bitmap, Configuration, Stop, FocusOnYou,
        FileChunkEnd,
        FileChunk,
        DropList
    }

    [StructLayout(LayoutKind.Sequential)]
    [Serializable]
    public struct ConfigurationData
    {
        public Int32 mouseConnPort;
        public Int32 keyboardConnPort;
    }

    [StructLayout(LayoutKind.Sequential)]
    [Serializable]
    public struct ClipboardData
    {
        public long Size;
        public byte[] Data;
        public string Filename;
        public string Password;
    }

    [StructLayout(LayoutKind.Explicit)]
    [Serializable]
    public struct Content
    {
        [FieldOffset(0)]
        public ClipboardData Clipboard;
        [FieldOffset(0)]
        public ConfigurationData Configuration;
        [FieldOffset(0)]
        public Boolean remoteDataAvaiable;
    }

    [StructLayout(LayoutKind.Sequential)]
    [Serializable]
    public struct AppData
    {
        public DataType Type;
        public Content Content;
    }

    class ClipboardAndConfigurationDataManager
    {
        private static BinaryFormatter formatter = new BinaryFormatter();
        private static bool FileIsCorrupted = false;

        public static Task<AppData> DeserializeAsync(AsyncClipboardConnection conn, Stream stream)
        {
            return Task.Run(() => {
                try
                {
                    AppData data = (AppData)formatter.Deserialize(stream);
                    if (data.Type == DataType.Stop)
                    {
                        Logger.Slog("Received stop request by client, clipboard");
                        conn.ClientStop = true;
                    }
                    return data;
                }
                catch (IOException)
                {
                    ///I enter here when 
                    ///1) father close connection. 
                    ///2) the connection is lost.
                    ///in any case set stopServer flag and exit
                    Logger.Slog("Connection lost.");
                    conn.ServerStop = true;
                    return new AppData();
                }
                catch (SerializationException)
                {
                    ///pacchetto corrotto o mi hanno inviato porcheria o client e server hanno differenti versioni della 
                    ///struttura INPUT.
                    ///NON so ancora bene come gestirlo: se si può scartare semplicemente il pacchetto o occorre chiudere lo stream
                    ///per ora chiudo la connessione per precauzione.
                    Logger.Slog("Connection lost. Stopping the Server.");
                    conn.ServerStop = true;
                    return new AppData();

                }
                catch(Exception e)
                {
                    //Altre eccezioni non desiderate, stoppo tutto lo stesso!
                    Logger.Slog("General exception on Deserialize Input (ClipboardAnd..DataManger:101); Message:\n" + e.Message);
                    conn.ServerStop = true;
                    return new AppData();
                }
            });
        }


        public static Task<bool> SerializeAsync(AppData data, Stream stream)
        {
            return Task.Run(() => {
                try
                {
                    //Logger.Slog("Invio messaggio");
                    formatter.Serialize(stream, data);
                    return true;
                }
                catch (IOException)
                {
                    ///Entro qua quando perdo connettività proprio quando sto inviando il messaggio di stop.
                    ///Non posso più effettuare l'operazione, quindi esco semplicemente, poichè ottengo lo 
                    ///stesso risultato quando il client sentirà la perdita di connessione ->chiuderà la connesione
                    Logger.Slog("Connection Lost.");
                    return false;
                }
                catch (SerializationException)
                {
                    ///Per qualche ragione non riesco a serializzare l'input, probabilemnte client e server hanno diverse
                    ///versioni della struttura INPUT (2 compilazioni diverse). Anche in questo caso il client se ne accorgerà
                    ///quando gli chiuderò in faccia la connessione.
                    Logger.Slog("Fail to Send message.");
                    return false;
                }
                catch (Exception e)
                {
                    ///Qualsiasi altra turba non mando nulla.
                    Logger.Slog("General exception on Deserialize Input (InputSerialization:137); Message:\n" + e.Message);
                    return false;
                }
            });
        }

        public static bool Serialize(AppData data, NetworkStream stream)
        {
            try
            {
                //Logger.Slog("Invio messaggio");
                formatter.Serialize(stream, data);
                return true;
            }
            catch (IOException)
            {
                ///Entro qua quando perdo connettività proprio quando sto inviando il messaggio di stop.
                ///Non posso più effettuare l'operazione, quindi esco semplicemente, poichè ottengo lo 
                ///stesso risultato quando il client sentirà la perdita di connessione ->chiuderà la connesione
                Logger.Slog("Connection Lost.");
                return false;
            }
            catch (SerializationException)
            {
                ///Per qualche ragione non riesco a serializzare l'input, probabilemnte client e server hanno diverse
                ///versioni della struttura INPUT (2 compilazioni diverse). Anche in questo caso il client se ne accorgerà
                ///quando gli chiuderò in faccia la connessione.
                Logger.Slog("Fail to Send message.");
                return false;
            }
            catch (Exception e)
            {
                ///Qualsiasi altra turba non mando nulla.
                Logger.Slog("General exception on Deserialize Input (InputSerialization:137); Message:\n" + e.Message);
                return false;
            }
        }

        internal static void ConfigurationHandler_onReceive(object sender, EventArgs e)
        {
            AsyncClipboardConnection conn = sender as AsyncClipboardConnection;
            
            AppData data = ((ByteBufferStream)e).AppData;

            short tryAgain = 0;
            string filePath;
            string tmpFilePath;

            switch (data.Type)
            {
                case DataType.Configuration:
                    //Here is possible get client to server configuration messageses if is needed.

                    break;

                case DataType.FocusOnYou:
                    if(data.Content.Clipboard.Size == 0) // lost focus
                    {
                        Logger.Slog("focus lost");
                        conn.RemoteConnection.App.Form.ChangeNotifyIconState(false);
                    }else
                    {
                        Logger.Slog("focus taken");
                        conn.RemoteConnection.App.Form.ChangeNotifyIconState(true);
                    }
                    break;

                case DataType.UnicodeText:
                    conn.RemoteConnection.App.Form.SetClipboardData(DataFormats.UnicodeText, Encoding.Unicode.GetString(data.Content.Clipboard.Data));
                    break;

                case DataType.ASCIIText:
                    conn.RemoteConnection.App.Form.SetClipboardData(DataFormats.Text, Encoding.Default.GetString(data.Content.Clipboard.Data));
                    break;

                case DataType.Bitmap:
                    MemoryStream ms = new MemoryStream(data.Content.Clipboard.Data);
                    Bitmap image = new Bitmap(Image.FromStream(ms));
                    conn.RemoteConnection.App.Form.SetClipboardData(DataFormats.Bitmap, image);
                    break;

                case DataType.Music:
                    conn.RemoteConnection.App.Form.SetClipboardData(DataFormats.WaveAudio, data);
                    break;

                case DataType.File:
                    filePath = Path.Combine(conn.RemoteConnection.App.LocalStoragePath, data.Content.Clipboard.Filename);

                    tryAgain = 0;
                    while (tryAgain < 3)
                    {
                        try
                        {
                            File.WriteAllBytes(filePath, data.Content.Clipboard.Data);
                            break;
                        }
                        catch (IOException ee)
                        {
                            Logger.Slog("Error while writing file " + conn.RemoteConnection.App.LocalStoragePath + data.Content.Clipboard.Filename + ".\nMessage:\n" + ee.Message);
                            tryAgain++;
                            Thread.Sleep(500);
                        }
                        catch(Exception ee)
                        {
                            Logger.Slog("Error while writing file " + conn.RemoteConnection.App.LocalStoragePath + data.Content.Clipboard.Filename + ".\nMessage:\n" + ee.Message);
                            break;
                        }
                    }
                    break;


                case DataType.FileChunk:
                    filePath = Path.Combine(conn.RemoteConnection.App.LocalStoragePath, data.Content.Clipboard.Filename);
                    tmpFilePath = filePath + ".tmp";

                    tryAgain = 0;
                    while (true)
                    {
                        try
                        {
                            using (var fileStream = new FileStream(tmpFilePath, FileMode.Append))
                            {
                                fileStream.Write(data.Content.Clipboard.Data, 0, data.Content.Clipboard.Data.Length);
                                break;
                            }
                        }
                        catch (IOException ee)
                        {
                            if (tryAgain >= 3)
                            {
                                FileIsCorrupted = true;
                                Logger.Slog("Error while writing file " + filePath + ".\nMessage:\n" + ee.Message);
                                break;
                            }
                            else
                            {
                                tryAgain++;
                                Thread.Sleep(500);
                            }
                        }
                        catch (Exception ee)
                        {
                            FileIsCorrupted = true;
                            Logger.Slog("Error while writing file " + filePath + ".\nMessage:\n" + ee.Message);
                            break;
                        }
                    }
                    break;


                case DataType.FileChunkEnd:
                    filePath = Path.Combine(conn.RemoteConnection.App.LocalStoragePath, data.Content.Clipboard.Filename);
                    tmpFilePath = filePath + ".tmp";

                    if (FileIsCorrupted || data.Content.Clipboard.Size == -2)
                    {
                        try
                        {
                            if (File.Exists(tmpFilePath))
                            {
                                File.Delete(tmpFilePath);
                            }
                            break;
                        }
                        catch (Exception)
                        {
                            ///Il file è corrotto ma non posso rimuoverlo.
                            ///C'è poco da fare... 
                            
                        }
                        finally
                        {
                            FileIsCorrupted = false;
                        }

                    }
                    else
                    {
                        try
                        {
                            using (var fileStream = new FileStream(tmpFilePath, FileMode.Append))
                            {
                                fileStream.Write(data.Content.Clipboard.Data, 0, data.Content.Clipboard.Data.Length);
                            }
                            if (File.Exists(filePath))
                            {
                                File.Delete(filePath);
                            }
                            File.Move(tmpFilePath, filePath);
                        }
                        catch (Exception)
                        {
                            ///Il file è sempre stato ok, all'ultimo chunk si è corrotto.
                            ///provo a chiuderlo ma se non riesco c'è poco da fare.
                            try
                            {
                                if (File.Exists(tmpFilePath))
                                {
                                    File.Delete(tmpFilePath);
                                }
                                break;
                            }
                            catch (Exception) { }
                        }
                    }
                    break;


                case DataType.Directory:
                    filePath = Path.Combine(conn.RemoteConnection.App.LocalStoragePath, data.Content.Clipboard.Filename);

                    tryAgain = 0;
                    while (true)
                    {
                        try
                        {
                            if (!Directory.Exists(filePath))
                            {
                                Directory.CreateDirectory(filePath);
                                Logger.Slog("server>> Create directory " + filePath);
                            }
                            break;
                        }catch(IOException)
                        {
                            if(tryAgain >= 3)
                            {
                                Logger.Slog("Error while creating directory " + filePath);
                                break;
                            }else
                            {
                                tryAgain++;
                                Thread.Sleep(500);
                            }
                        }catch(Exception ee)
                        {
                            Logger.Slog("Error while writing directory " + filePath + ".\nMessage:\n" + ee.Message);
                            break;
                        }
                    }
                    break;
                case DataType.DropList:
                    Logger.Slog("Put File in clipboard!");
                    conn.RemoteConnection.App.Form.SetClipboardData(DataFormats.FileDrop, 
                        FileManager.FromBaseNameToStringColletion(
                            conn.RemoteConnection.App.LocalStoragePath,data.Content.Clipboard.Filename));
                    break;

                default:
                    //TODO
                    break;
            }
        }
        
    }

}
