﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using RemoteController_server.Debug;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Collections;

namespace RemoteController_server.model.implementations
{
    
    [StructLayout(LayoutKind.Sequential)]
    [Serializable]
    public class POINT
    {
        public int x;
        public int y;
    }

    //Declare the wrapper managed MSLLHOOKSTRUCT class.
    /// <summary>
    /// https://msdn.microsoft.com/en-us/library/windows/desktop/ms644970(v=vs.85).aspx
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    [Serializable]
    public class MSLLHOOKSTRUCT
    {
        public POINT pt;            // the x and y coordinates of the cursor
        public uint mouseData;      // mouse buttons informations
        public int flags;           // event flags
        public int time;            // timestamp of the message
        public int dwExtraInfo;     // extra infomration
    }

    /// <summary>
    /// https://msdn.microsoft.com/it-it/library/windows/desktop/ms644967%28v=vs.85%29.aspx?f=255&MSPPError=-2147217396
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    [Serializable]
    public class KBDLLHOOKSTRUCT
    {
        public uint vkCode;         // virtual-key code
        public uint scanCode;       // hardware scan code
        public uint flags;          // extended-key flag
        public uint time;           // timestamp of this msg
        public UIntPtr dwExtraInfo;   // additional information
    }

    public enum MouseFlags
    {
        MOUSEEVENTF_ABSOLUTE = 0x8000,
        MOUSEEVENTF_MOVE = 0x0001,
        MOUSEEVENTF_LEFTDOWN = 0x0002,
        MOUSEEVENTF_LEFTUP = 0x0004,
        MOUSEEVENTF_RIGHTDOWN = 0x0008,
        MOUSEEVENTF_RIGHTUP = 0x0010,
        MOUSEEVENTF_MIDDLEDOWN = 0x0020,
        MOUSEEVENTF_MIDDLEUP = 0x0040,
        MOUSEEVENTF_VIRTUALDESK = 0x4000,
        MOUSEEVENTF_WHEEL = 0x0800
    }
    public enum MouseMessages
    {
        WM_LBUTTONDOWN = 0x0201,
        WM_LBUTTONUP = 0x0202,
        WM_MOUSEMOVE = 0x0200,
        WM_MOUSEWHEEL = 0x020A,
        WM_RBUTTONDOWN = 0x0204,
        WM_RBUTTONUP = 0x0205
    }
    public enum KeyboardFlags
    {
        KEYEVENTF_EXTENDEDKEY = 0x0001,
        KEYEVENTF_KEYUP = 0x0002,
        KEYEVENTF_SCANCODE = 0x0008,
        KEYEVENTF_UNICODE = 0x0004
    }
    public enum KeyboardMessages
    {
        WM_KEYDOWN = 0x0100,
        WM_KEYUP = 0x0101,
        WM_SYSKEYDOWN = 0x0104,
        WM_SYSKEYUP = 0x0105
    }

    /// <summary>
    /// https://msdn.microsoft.com/it-it/library/windows/desktop/ms646270(v=vs.85).aspx
    /// </summary>
    public enum InputTypes
    {
        INPUT_MOUSE = 0,
        INPUT_KEYBOARD = 1,
        INPUT_HARDWARE = 2,
        INPUT_STOP = 3 //CUSTOM
    }

    /// <summary>
    /// http://msdn.microsoft.com/en-us/library/windows/desktop/ms646270(v=vs.85).aspx
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    [Serializable]
    public struct INPUT
    {
        public uint Type;
        public MOUSEKEYBDHARDWAREINPUT Data;
    }

    /// <summary>
    /// http://social.msdn.microsoft.com/Forums/en/csharplanguage/thread/f0e82d6e-4999-4d22-b3d3-32b25f61fb2a
    /// </summary>
    [StructLayout(LayoutKind.Explicit)]
    [Serializable]
    public struct MOUSEKEYBDHARDWAREINPUT
    {
        [FieldOffset(0)]
        public HARDWAREINPUT Hardware;
        [FieldOffset(0)]
        public KEYBDINPUT Keyboard;
        [FieldOffset(0)]
        public MOUSEINPUT Mouse;
    }

    /// <summary>
    /// http://msdn.microsoft.com/en-us/library/windows/desktop/ms646310(v=vs.85).aspx
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    [Serializable]
    public struct HARDWAREINPUT
    {
        public uint Msg;
        public ushort ParamL;
        public ushort ParamH;
    }

    /// <summary>
    /// http://msdn.microsoft.com/en-us/library/windows/desktop/ms646310(v=vs.85).aspx
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    [Serializable]
    public struct KEYBDINPUT
    {
        public ushort Vk;
        public ushort Scan;
        public uint Flags;
        public uint Time;
        public IntPtr ExtraInfo;
    }

    /// <summary>
    /// http://social.msdn.microsoft.com/forums/en-US/netfxbcl/thread/2abc6be8-c593-4686-93d2-89785232dacd
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    [Serializable]
    public struct MOUSEINPUT
    {
        public int X;
        public int Y;
        public int MouseData;
        public uint Flags;
        public uint Time;
        public IntPtr ExtraInfo;
    }

    public class InputManager
    {
        public const int WH_KEYBOARD_LL = 13;
        public const int WH_KEYBOARD = 2;
        public const uint FIRSTNUMBERCODE = 0x30;
        public const uint LASTNUMBERCODE = 0X39;
        public const uint VK_CONTROL = 0x11;
        public const uint VK_LCONTROL = 0xA2;
        public const uint VK_RCONTROL = 0xA3;
        public const uint VK_MENU = 0x12;
        public const uint VK_LMENU = 0xA4;
        public const uint VK_RMENU = 0xA5;
        public const int A_KEY = 0x41;
        public const int Z_KEY = 0x5A;

        const int CXSCREEN = 0;
        const int CYSCREEN = 1;
        static public String getInfosFromInput(INPUT input)
        {
            return "type: " + input.Type + ", x = " + input.Data.Mouse.X + " y = " + input.Data.Mouse.Y +
                ", mouseData = " + input.Data.Mouse.MouseData + ", flags = " + input.Data.Mouse.Flags;

        }
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern int GetSystemMetrics(int nIndex);

        static public INPUT getInputFromMouseHook(MSLLHOOKSTRUCT mouseHook, int msg, ref bool toSend)
        {
            toSend = true;
            INPUT inputPacket = new INPUT();
            inputPacket.Type = (int)InputTypes.INPUT_MOUSE;
            
            inputPacket.Data.Mouse = new MOUSEINPUT();

            inputPacket.Data.Mouse.X = mouseHook.pt.x * 65536 / GetSystemMetrics(CXSCREEN);
            inputPacket.Data.Mouse.Y = mouseHook.pt.y * 65536 / GetSystemMetrics(CYSCREEN);

            inputPacket.Data.Mouse.MouseData = 0;
            uint flags = (uint) MouseFlags.MOUSEEVENTF_ABSOLUTE;
            flags |= (uint)MouseFlags.MOUSEEVENTF_VIRTUALDESK;

            switch ((MouseMessages)msg)
            {
                case MouseMessages.WM_LBUTTONDOWN:
                    //Logger.wr("left button down");
                    flags |= (uint)MouseFlags.MOUSEEVENTF_LEFTDOWN; 
                    break;
                case MouseMessages.WM_RBUTTONDOWN:
                    //Logger.wr("right button down");
                    flags |= (uint)MouseFlags.MOUSEEVENTF_RIGHTDOWN;
                    break;
                case MouseMessages.WM_MOUSEWHEEL:
                    //Logger.wr("mousewheel");
                    flags |= (uint)MouseFlags.MOUSEEVENTF_WHEEL;
                    //Console.WriteLine(mouseHook.mouseData);
                    short delta = (short)(mouseHook.dwExtraInfo >> 16);
                    //Logger.wr(delta.ToString());
                    //Logger.wr(mouseHook.dwExtraInfo.ToString());
                    inputPacket.Data.Mouse.MouseData = mouseHook.dwExtraInfo >> 16;
                    break;
                case MouseMessages.WM_LBUTTONUP:
                    //Logger.wr("left button up");
                    flags |= (uint)MouseFlags.MOUSEEVENTF_LEFTUP;
                    break;
                case MouseMessages.WM_RBUTTONUP:
                    //Logger.wr("right button up");
                    flags |= (uint)MouseFlags.MOUSEEVENTF_RIGHTUP;
                    break;
                case MouseMessages.WM_MOUSEMOVE:
                    //Logger.wr("mouve");
                    flags |= (uint)MouseFlags.MOUSEEVENTF_MOVE;
                    break;
                default:
                    //Logger.wr("dunno");
                    toSend = false;
                    break;
            }
            inputPacket.Data.Mouse.Flags = flags;
            //inputPacket.mi.time = 0;
            // TODO WHEEL BUTTON
            

            return inputPacket;
        }

        static public INPUT getInputFromKeyboardHook(KBDLLHOOKSTRUCT keyboardHook, int msg)
        {
            INPUT input = new INPUT();
            input.Type = (int)InputTypes.INPUT_KEYBOARD;
            input.Data.Keyboard.Vk = (ushort) keyboardHook.vkCode;
            input.Data.Keyboard.Scan = (ushort) keyboardHook.scanCode;
            input.Data.Keyboard.Flags = 0;
            switch ((KeyboardMessages)msg)
            {
                case KeyboardMessages.WM_KEYUP:
                case KeyboardMessages.WM_SYSKEYUP:
                    input.Data.Keyboard.Flags |= (uint)KeyboardFlags.KEYEVENTF_KEYUP;
                    break;
            }
            //Logger.wr("Vk " + input.Data.Keyboard.Vk.ToString() + "Scan " + input.Data.Keyboard.Scan.ToString() + "flags " + keyboardHook.flags.ToString());
            return input;

        }

        public static byte[] getBytesFromInput(INPUT input, ref int size)
        {
            size = Marshal.SizeOf(input);
            byte[] arr = new byte[size];

            IntPtr ptr = Marshal.AllocHGlobal(size);
            Marshal.StructureToPtr(input, ptr, true);
            Marshal.Copy(ptr, arr, 0, size);
            Marshal.FreeHGlobal(ptr);
            return arr;
        }
        public static INPUT getInputFromBytes(byte[] bytes)
        {
            INPUT input = new INPUT();
            int size = Marshal.SizeOf(input);
            IntPtr ptr = Marshal.AllocHGlobal(size);
            Marshal.Copy(bytes, 0, ptr, size);
            input = (INPUT)Marshal.PtrToStructure(ptr, input.GetType());
            Marshal.FreeHGlobal(ptr);
            return input;
        }

        public static IEnumerable<INPUT> GetListOfReleasedModifierInput(bool alt, bool ctrl)
        {
            List<INPUT> inputList = new List<INPUT>();
            if (alt)
            {
                INPUT altReleased = new INPUT();
                altReleased.Type = (uint)InputTypes.INPUT_KEYBOARD;
                altReleased.Data.Keyboard.Flags = (uint)KeyboardFlags.KEYEVENTF_KEYUP;
                altReleased.Data.Keyboard.Vk = (ushort)VK_MENU;
                altReleased.Data.Keyboard.Scan = 56;
                inputList.Add(altReleased);

                INPUT altLeftReleased = new INPUT();
                altLeftReleased.Type = (uint)InputTypes.INPUT_KEYBOARD;
                altLeftReleased.Data.Keyboard.Flags = (uint)KeyboardFlags.KEYEVENTF_KEYUP;
                altLeftReleased.Data.Keyboard.Vk = (ushort)VK_LMENU;
                altLeftReleased.Data.Keyboard.Scan = 56;
                inputList.Add(altLeftReleased);

                INPUT altRightReleased = new INPUT();
                altRightReleased.Type = (uint)InputTypes.INPUT_KEYBOARD;
                altRightReleased.Data.Keyboard.Flags = (uint)KeyboardFlags.KEYEVENTF_KEYUP;
                altRightReleased.Data.Keyboard.Vk = (ushort)VK_RMENU;
                altRightReleased.Data.Keyboard.Scan = 56;
                inputList.Add(altRightReleased);
            }
            if (ctrl)
            {
                INPUT ctrlReleased = new INPUT();
                ctrlReleased.Type = (uint)InputTypes.INPUT_KEYBOARD;
                ctrlReleased.Data.Keyboard.Flags = (uint)KeyboardFlags.KEYEVENTF_KEYUP;
                ctrlReleased.Data.Keyboard.Vk = (ushort)VK_CONTROL;
                ctrlReleased.Data.Keyboard.Scan = 29;
                inputList.Add(ctrlReleased);

                INPUT ctrlLeftReleased = new INPUT();
                ctrlLeftReleased.Type = (uint)InputTypes.INPUT_KEYBOARD;
                ctrlLeftReleased.Data.Keyboard.Flags = (uint)KeyboardFlags.KEYEVENTF_KEYUP;
                ctrlLeftReleased.Data.Keyboard.Vk = (ushort)VK_LCONTROL;
                ctrlLeftReleased.Data.Keyboard.Scan = 29;
                inputList.Add(ctrlLeftReleased);

                INPUT ctrlRightReleased = new INPUT();
                ctrlRightReleased.Type = (uint)InputTypes.INPUT_KEYBOARD;
                ctrlRightReleased.Data.Keyboard.Flags = (uint)KeyboardFlags.KEYEVENTF_KEYUP;
                ctrlRightReleased.Data.Keyboard.Vk = (ushort)VK_RCONTROL;
                ctrlRightReleased.Data.Keyboard.Scan = 29;
                inputList.Add(ctrlRightReleased);
            }
            return inputList;
        }
    }
}
