﻿using RemoteController_server.Debug;
using RemoteController_server.model.implementations.marshalling_utils;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RemoteController_server.model.implementations.clipboard_utils
{
    class ClipboardManager
    {
        ///Classe che si occupa di gestire la clipboard.
        ///L'idea è questa:  il server installa un listener della clipboard,
        ///ogni volta che cambia il suo contenuto, esso viene inviato anche al client
        ///a meno che non sia enorme, in questo cao occorre una conferma.
        ///quando si pasa il focus su un altro server si innietta il dato nella 
        ///clipboard del server, se è grande (es. file grande) si notifica solo la
        ///presenza e con una conbinazione di tasti la si innietta.

        private const int MAXFILESIZEALERT = 3000000;


        /// <summary>
        /// Places the given window in the system-maintained clipboard format listener list.
        /// </summary>
        [DllImport("user32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool AddClipboardFormatListener(IntPtr hwnd);

        /// <summary>
        /// Removes the given window from the system-maintained clipboard format listener list.
        /// </summary>
        [DllImport("user32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool RemoveClipboardFormatListener(IntPtr hwnd);

        /// <summary>
        /// Sent when the contents of the clipboard have changed.
        /// </summary>
        private const int WM_CLIPBOARDUPDATE = 0x031D;

        private RemoteConnection_server conn;
        private FileManager fileManager;
        private IntPtr handle;

        public ClipboardManager(IntPtr handle, RemoteConnection_server conn)
        {
            this.handle = handle;
            this.conn = conn;
            fileManager = new FileManager(conn.ClipboardConnection);
        }

        public void Start()
        {
            Logger.Slog("inserisco c listener");
            if (!AddClipboardFormatListener(handle))
                MessageBox.Show("AddClipboardFormatListener failed");
        }

        public async void ElaborateClipboard()
        {
            IDataObject iData = Clipboard.GetDataObject();      // Clipboard's data.

            foreach (String s in iData.GetFormats())
                Console.WriteLine("E' presente: " + s);
            Console.WriteLine("\n\n");

            if (conn.Status == ServerConnectionStatus.Connected)
            {                
                AppData clbData = new AppData();

                /* Depending on the clipboard's current data format we can process the data differently. 
                 * Feel free to add more checks if you want to process more formats. */

                if (iData.GetDataPresent(DataFormats.UnicodeText))
                {
                    Byte[] encodedBytes = Encoding.Unicode.GetBytes((String)iData.GetData(DataFormats.UnicodeText));
                    clbData.Content.Clipboard.Data = encodedBytes;
                    clbData.Type = DataType.UnicodeText;
                    conn.ClipboardConnection.SendClipboardData(clbData);
                }
                else if (iData.GetDataPresent(DataFormats.Text))
                {
                    string text = (string)iData.GetData(DataFormats.Text);
                    //Console.WriteLine(text);
                    ///Invia il testo
                    byte[] bytes = Encoding.ASCII.GetBytes(text);
                    clbData.Content.Clipboard.Data = bytes;
                    clbData.Type = DataType.ASCIIText;
                    conn.ClipboardConnection.SendClipboardData(clbData);

                }
                else if (iData.GetDataPresent(DataFormats.Bitmap))
                {
                    Bitmap image = (Bitmap)iData.GetData(DataFormats.Bitmap);
                    Console.WriteLine("IMMAGINE!");
                    if (image != null)
                    {
                        using (MemoryStream ms = new MemoryStream())
                        {
                            image.Save(ms, ImageFormat.Bmp);
                            clbData.Content.Clipboard.Data = ms.ToArray();
                        }
                        clbData.Type = DataType.Bitmap;
                        conn.ClipboardConnection.SendClipboardData(clbData);
                    }
                }
                else if (iData.GetDataPresent(DataFormats.FileDrop))
                {
                    String[] fileDrop = (String[])iData.GetData(DataFormats.FileDrop);

                    foreach (String file in fileDrop)
                    {
                        Console.WriteLine("file: " + file);
                        long fileSize = await fileManager.CalculateAsync(file);

                        if (fileSize > MAXFILESIZEALERT)
                        {
                            var confirmResult = MessageBox.Show("The file is big ( "+ fileSize +" >2Mb).\n Do you wanna Remote Copy it anyway?",
                                     "Confirmation",
                                     MessageBoxButtons.YesNo);
                            if (confirmResult != DialogResult.Yes)
                            {
                                return;
                            }
                        }

                        await fileManager.ManagePathAsync(file);
                        clbData.Type = DataType.DropList;
                    }
                    int count = 3;
                    while (count > 0)
                    {
                        try
                        {
                            clbData.Content.Clipboard.Filename = FileManager.FromAPathsListToBaseName(fileDrop);
                            conn.ClipboardConnection.SendClipboardData(clbData);
                            return;
                        }
                        catch (Exception)
                        {
                            Logger.Slog("File not found on localStorage.\nOperation aborting...");
                            count--;
                        }
                    }
                }
            }
        }

        public void Stop()
        {
            fileManager.removeAllChunks(conn.App.LocalStoragePath);
            RemoveClipboardFormatListener(handle);     // Remove our window from the clipboard's format listener list.
        }

    }
}
