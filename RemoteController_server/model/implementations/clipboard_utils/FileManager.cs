﻿using RemoteController_server.model.implementations.marshalling_utils;
using RemoteController_server.model.interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections.Specialized;

namespace RemoteController_server.model.implementations.clipboard_utils
{
    public class FileManager
    {
        private const int MAXVALUEFULLFILE = 200 * 1024 * 1024;
        private const int CHUNKSIZE = 200 * 1024 * 1024;


        private IClipboardSender sender;

        public IClipboardSender Sender { get { return sender; } set { sender = value; } }

        public FileManager()
        {
        }
        public FileManager(IClipboardSender sender)
        {
            this.sender = sender;
        }

        public Task ManagePathAsync(String path)
        {
            return Task.Run(()=> {
                try
                {
                    FileAttributes attr = File.GetAttributes(path);

                    if ((attr & FileAttributes.Directory) == FileAttributes.Directory)
                    {
                        DirectoryInfo dir = new DirectoryInfo(path);
                        this.SendDirectoryContent(path, dir.Name);
                    }
                    else
                    {
                        FileInfo file = new FileInfo(path);
                        this.SendFile(path, file.Name);
                    }
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                    return;
                }
            });
           
        }
        private void SendDirectoryContent(string absolutePath, string relativePath)
        {
            DirectoryInfo dir = new DirectoryInfo(absolutePath);

            if (!dir.Exists)
            {
                //throw new DirectoryNotFoundException(
                //    "Source directory does not exist or could not be found: " + absolutePath);
                return;
            }

            SendDirectory(relativePath);

            // Get the files in the directory and copy them to the new location.
            FileInfo[] files = dir.GetFiles();
            DirectoryInfo[] dirs = dir.GetDirectories();

            foreach (FileInfo file in files)
            {
                string absoluteFileName = Path.Combine(absolutePath, file.Name);
                string relativeFileName = Path.Combine(relativePath, file.Name);
                SendFile(absoluteFileName, relativeFileName);
            }

            foreach (DirectoryInfo subdir in dirs)
            {
                string absoluteDirectoryName = Path.Combine(absolutePath, subdir.Name);
                string relativeDirectoryName = Path.Combine(relativePath, subdir.Name);
                SendDirectoryContent(absoluteDirectoryName, relativeDirectoryName);
            }

        }

        private void SendDirectory(String dirname)
        {
            AppData data = new AppData();
            data.Content.Clipboard.Filename = dirname;
            data.Type = DataType.Directory;
            sender.SendClipboardData(data);
        }

        private void SendFile(string absoluteFileName, string relativeFileName)
        {
            AppData clbData = new AppData();
            FileInfo infos = new FileInfo(absoluteFileName);
            //Console.WriteLine("dim: " + infos.Length);
            //Console.WriteLine("absoluteFileName: " + absoluteFileName);
            //Console.WriteLine("relativeFileName: " + relativeFileName);

            if (infos.Length <= MAXVALUEFULLFILE)
            {
                clbData.Content.Clipboard.Data = File.ReadAllBytes(absoluteFileName);
                clbData.Type = DataType.File;
                clbData.Content.Clipboard.Size = infos.Length;
                clbData.Content.Clipboard.Filename = relativeFileName;
                sender.SendClipboardData(clbData);
            }else
            {
                try
                {
                    using (var file = File.OpenRead(absoluteFileName))
                    {
                        byte[] buffer = new byte[CHUNKSIZE];
                        
                        for (int chunkIndex = 0; ; chunkIndex++)
                        {

                            int bytesAlreadyRead = 0;
                            int byteRead;
                            while ((byteRead = file.Read(buffer,
                                bytesAlreadyRead,
                                CHUNKSIZE - bytesAlreadyRead)) > 0)
                            {
                                bytesAlreadyRead += byteRead;
                            }
                            //invio il chunk!
                            clbData.Content.Clipboard.Data = buffer;
                            clbData.Content.Clipboard.Size = bytesAlreadyRead;
                            clbData.Content.Clipboard.Filename = relativeFileName;
                            if (bytesAlreadyRead != CHUNKSIZE)
                            {
                                clbData.Type = DataType.FileChunkEnd;
                                sender.SendClipboardData(clbData);
                                break;
                            }
                            clbData.Type = DataType.FileChunk;
                            sender.SendClipboardData(clbData);
                        }
                    }
                }
                catch (Exception)
                {
                    clbData.Content.Clipboard.Size = -2;
                    clbData.Content.Clipboard.Filename = relativeFileName;
                    clbData.Type = DataType.FileChunkEnd;
                    sender.SendClipboardData(clbData);
                }
            }
        }

        internal Task<long> CalculateAsync(string path)
        {
            return Task.Run(()=>
            {
                FileAttributes attr = File.GetAttributes(path);

                if ((attr & FileAttributes.Directory) == FileAttributes.Directory)
                {
                    DirectoryInfo dir = new DirectoryInfo(path);
                    return CalculateDirectoryContentSize(path, 0);
                }
                else
                {
                    FileInfo file = new FileInfo(path);
                    return file.Length;
                }
                
            });
        }

        private long CalculateDirectoryContentSize(string absolutePath, long size)
        {
            DirectoryInfo dir = new DirectoryInfo(absolutePath);

            if (!dir.Exists) //TODO: catch
            {
                //throw new DirectoryNotFoundException(
                //    "Source directory does not exist or could not be found: " + absolutePath);
                return 0;
            }

            // Get the files in the directory and copy them to the new location.
            FileInfo[] files = dir.GetFiles();
            DirectoryInfo[] dirs = dir.GetDirectories();

            foreach (FileInfo file in files)
            {
                size += file.Length;
            }

            foreach (DirectoryInfo subdir in dirs)
            {
                string absoluteDirectoryName = Path.Combine(absolutePath, subdir.Name);
                size += CalculateDirectoryContentSize(absoluteDirectoryName, size);
            }
            return size;
        }

        public void removeAllChunks(string absolutePath)
        {
            DirectoryInfo dir = new DirectoryInfo(absolutePath);

            if (!dir.Exists)
            {
                return;
            }

            // Get the files in the directory and copy them to the new location.
            FileInfo[] files = dir.GetFiles();
            DirectoryInfo[] dirs = dir.GetDirectories();

            foreach (FileInfo file in files)
            {
                if (file.Name.EndsWith(".tmp"))
                {
                    File.Delete(file.FullName);
                }
            }

            foreach (DirectoryInfo subdir in dirs)
            {
                string absoluteDirectoryName = Path.Combine(absolutePath, subdir.Name);
                removeAllChunks(absoluteDirectoryName);
            }
        }

        public static string FromAPathsListToBaseName(string[] apaths)
        {
            string res = "";
            for(int i = 0; i < apaths.Length; i++)
            {
                FileAttributes attr = File.GetAttributes(apaths[i]);
                if ((attr & FileAttributes.Directory) == FileAttributes.Directory)
                {
                    DirectoryInfo dir = new DirectoryInfo(apaths[i]);
                    if (i == apaths.Length - 1)
                    {
                        res += dir.Name;
                    }
                    else
                    {
                        res += dir.Name + "|";
                    }
                }
                else
                {
                    FileInfo file = new FileInfo(apaths[i]);
                    if (i == apaths.Length - 1)
                    {
                        res += file.Name;
                    }
                    else
                    {
                        res += file.Name + "|";
                    }
                }
            }
            return res;
        }

        internal static StringCollection FromBaseNameToStringColletion(string localStoragePath, string baseFile)
        {
            StringCollection coll = new StringCollection();
            foreach(string rpath in FromBaseNameToRPaths(baseFile))
            {
                coll.Add(Path.Combine(localStoragePath, rpath));
            }
            return coll;
        }

        public static string[] FromBaseNameToRPaths(string baseName)
        {
            return baseName.Split('|');
        }
    } 
}
