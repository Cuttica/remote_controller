﻿using RemoteController_server.Debug;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RemoteController_server.model.implementations
{

    class AsyncUdpServerConnection : UdpServerConnection
    {
        private CancellationTokenSource stopServerCancelSource;

        public AsyncUdpServerConnection(RemoteConnection_server remoteConnection, int port) : base(remoteConnection,port)
        {
        }


        /// <summary>
        /// Run a UDPClient that processes the client incoming data (mouse data).
        /// If internet expires a SocketException is catch and it closes the server
        /// application: clipboard listener, clipboard and keyboard connection.
        /// </summary>
        public override async void Run()
        {
            Init();
            // The CancellationToken.
            stopServerCancelSource = new CancellationTokenSource();
            var cancellationToken = stopServerCancelSource.Token;
            while (!stopReceveing)
            {
                try
                {
                    UdpReceiveResult result;
                    isReceiving = true;
                    result = await server.ReceiveAsync().WithWaitCancellation(cancellationToken, result);
                    if (stopServerCancelSource.IsCancellationRequested)
                    {
                        Logger.Slog("Richiesta stop Receiving");
                        server.Close();
                        return;
                    }
                    TcpClient client = remoteConnection.KeyboardConnection.CLient;

                    if (client != null && client.Connected && remoteConnection.Status == ServerConnectionStatus.Connected && 
                        ((IPEndPoint)client.Client.RemoteEndPoint).Address.Equals(result.RemoteEndPoint.Address))
                    {
                        Byte[] receiveBytes = result.Buffer;
                        base.OnReceive(new ByteBufferStream(receiveBytes));
                    }
                }
                catch (SocketException)
                {
                    Logger.Slog("Connection Lost. (AsyncUdpServerConnection:54)");
                    //chiudo il server (listener + connection);
                    remoteConnection.App.Stop();
                    return;
                }
                catch (InvalidOperationException)
                {
                    Logger.Slog("Impossible to put data input on System Input Queue, wrong data format or wrong permission.");
                    remoteConnection.App.Stop();
                    return;
                }
            }
        }
        public override void CloseConnection()
        {
            if (isReceiving)
            {
                stopServerCancelSource.Cancel();
            }
            isReceiving = false;            
        }

        public override void CloseClientConnection()
        {
            if (isReceiving)
            {
                stopServerCancelSource.Cancel();
            }
            isReceiving = false;
        }
    }
}
