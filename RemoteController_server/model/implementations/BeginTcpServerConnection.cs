﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using System.Windows.Forms;
using RemoteController_server.Debug;
using System.Threading;
using System.Runtime.InteropServices;
using System.IO;

namespace RemoteController_server.model.implementations
{
    class BeginTcpServerConnection : TcpServerConnection
    {
        private bool stopListening = false;

        public BeginTcpServerConnection(IPAddress addr, int port) : base(addr, port) {}

        private ManualResetEvent endToReceive = new ManualResetEvent(false);

        private int bytesToRead;
        byte[] buffer;

        public override void Run()
        {
            try
            {
                StartListening();
                try
                {
                    AcceptAsync();
                }
                catch (SocketException e)
                {
                    //MessageBox.Show("Invalid IPAddress.");
                    Logger.wr("Invalid IPAddress!");
                    throw e;
                }
            }
            catch (Exception)
            {
                Console.WriteLine("esco");
            }
        }

        private void AcceptAsync()
        {
            Console.WriteLine("Thread {0} >> Start Accepting", Thread.CurrentThread.ManagedThreadId);
            server.BeginAcceptTcpClient(
                    new AsyncCallback(AcceptCallback),
                    server);
        }

        private void AcceptCallback(IAsyncResult ar)
        {
            Console.WriteLine("Thread {0} >> Handling the new connection.", Thread.CurrentThread.ManagedThreadId);
            
            // Get the socket that handles the client request.
            TcpListener listener = (TcpListener)ar.AsyncState;

            if (stopListening)
            {
                Console.WriteLine("Thread {0} >> Receiving a \"ServerStop listening request\" by server side user; Closing.", Thread.CurrentThread.ManagedThreadId);
                return;
            }
            TcpClient handler = listener.EndAcceptTcpClient(ar);

            status = ServerConnectionStatus.Connected;
            INPUT input = new INPUT();
            int size = Marshal.SizeOf(input);
            buffer = new byte[size];
            bytesToRead = size;
            //handler.GetStream().BeginRead(buffer, 0, size, ReadCallback, handler.GetStream());
            BeingReading(handler.GetStream(), buffer);
            //while (!serverStop)
            //{
            //    if (handler.Connected)
            //        base.OnReceive(new ByteBufferStream(handler.GetStream()));
            //    else
            //        Console.WriteLine("sfsdfasdfsadf");
            //}
            //Console.WriteLine("Thread {0} >> Connetion stop by server side user: Closing.", Thread.CurrentThread.ManagedThreadId);
            //handler.Close();

            //serverStop = false;
            //stopListening = false;
        }
        private void BeingReading(NetworkStream stream, byte[] buffer)
        {
            if (!serverStop)
            {
                stream.BeginRead(buffer, 0, bytesToRead, new AsyncCallback(ReadCallback), stream);
            }
        }
        private void ReadCallback(IAsyncResult ar)
        {
            NetworkStream stream = (NetworkStream)ar.AsyncState;
            int nread = stream.EndRead(ar);
            Console.WriteLine("letti: " + nread);
            if(nread == 0)
            {
                //end of comunication -> disconnect;
                return;
            }

            Console.WriteLine("Thread {0} >> Received an entire INPUT struct.", Thread.CurrentThread.ManagedThreadId);
            base.OnReceive(new ByteBufferStream(buffer));// new MemoryStream(buffer)));

        }

        public override void CloseClientConnection()
        {
            Console.WriteLine("Thread {0} >> Async CloseClientConnection: close connection and accept other connections.", Thread.CurrentThread.ManagedThreadId);
            switch (status)
            {
                case ServerConnectionStatus.Connected:                    
                    serverStop = true;
                    break;
                case ServerConnectionStatus.Listening:
                    stopListening = true;
                    break;
            }
            status = ServerConnectionStatus.Disconnected;
            //AcceptAsync();
        }

        public override void CloseConnection()
        {
            Console.WriteLine("Thread {0} >> Async CloseServerConnection;", Thread.CurrentThread.ManagedThreadId);
            switch (status)
            {
                case ServerConnectionStatus.Connected:
                    Console.WriteLine("stopComunication = true;");
                    serverStop = true;
                    break;
                case ServerConnectionStatus.Listening:
                    stopListening = true;
                    break;
            }
            server.Stop();
            status = ServerConnectionStatus.Disconnected;
        }
    }

}
