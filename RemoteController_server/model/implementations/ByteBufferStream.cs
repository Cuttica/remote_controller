﻿using RemoteController_server.model.implementations.marshalling_utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RemoteController_server.model.implementations
{
    public class ByteBufferStream : EventArgs
    {
        private byte[] data;
        private Stream stream;
        private INPUT input;
        private AppData appData;

        public ByteBufferStream(Stream s)
        {
            stream = s;
        }
        public ByteBufferStream(byte[] d)
        {
            data = d;
        }

        public ByteBufferStream(INPUT i)
        {
            input = i;
        }

        public ByteBufferStream(AppData a)
        {
            appData = a;
        }
        public byte[] Data { get { return data; } set { data = value; } }
        public Stream Stream { get { return stream; } set { stream = value; } }
        public INPUT Input { get { return input; } }
        public AppData AppData { get { return appData; } }
    }
}
