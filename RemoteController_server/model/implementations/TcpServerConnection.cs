﻿using RemoteController_server.Debug;
using RemoteController_server.model.implementations.hook_utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace RemoteController_server.model.implementations
{

    class TcpServerConnection : IConnection
    {
        protected RemoteConnection_server remoteConnection;
        protected ServerConnectionStatus status;
        protected TcpListener server;
        protected IPAddress localAddress;
        protected Int32 port;
        protected TcpClient client;
        protected volatile bool serverStop;
        protected volatile bool clientStop;

        public event DataHandler onReceive;

        public TcpClient CLient { get { return client; } }
        public RemoteConnection_server RemoteConnection { get { return remoteConnection; } }
        public ServerConnectionStatus Status { get { return status; }}
        public IPAddress LocalAddress { get { return localAddress; } set { if (value != null) localAddress = value; } }
        public Int32 Port { get { return port; } set { port = value; } }
        public bool ServerStop { get { return serverStop; } set { serverStop = value; } }
        public bool ClientStop { get { return clientStop; } set { clientStop = value; } }

        /// <summary>
        ///  Creates a simple TcpServerConnection.
        /// </summary>
        /// <param name="addr">The IP local address where listen to.</param>
        /// <param name="port">The port where listen to.</param>
        /// <exception cref="ArgumentNullException" >Raised when addr is null.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Raised when port is not between <c>MinPort</c> and <c>MaxPort</c>.</exception>
        public TcpServerConnection(RemoteConnection_server rc, IPAddress addr, int port = 0)
        {            
            this.port = port;
            this.serverStop = false;
            this.localAddress = addr;
            this.status = ServerConnectionStatus.Disconnected;
            this.remoteConnection = rc;
        }

        /*
        private int FreeTcpPort()
        {
            TcpListener l = new TcpListener(IPAddress.Loopback, 0);
            l.Start();
            int port = ((IPEndPoint)l.LocalEndpoint).Port;
            l.ServerStop();
            return port;
        }
        */
        public virtual void StartListening()
        {
            server = new TcpListener(localAddress, port);
            try
            {
                server.Start();
                status = ServerConnectionStatus.Listening;
                port = ((IPEndPoint)server.LocalEndpoint).Port;
                Logger.Slog("In ascolto su: " + ((IPEndPoint)server.LocalEndpoint).Address + ":" + port);
            }
            catch (SocketException e)
            {
                Logger.wr("Maximum number of connections reached, closing server!");
                remoteConnection.App.Stop();
                return;
            }
            // TODO Searching Bar start()
        }

        /// <summary>
        /// Accept one connection from client, wait for messages and run delegate <c>onReceive</c> when it receives them.
        /// <exception cref="InvalidOperationException">Raised if <c>StartConnection()</c> is called before calling <c>Init()</c>.</exception>.
        /// <exception cref="SocketException">Potentially raised by <c>AcceptTcpClient()</c>.</exception>
        /// <exception cref="InvalidOperationException">Raised if the server is not longer connected with the client.</exception>
        /// <exception cref="ObjectDisposedException">Raised if the tcp connection has been closed or an failure reading from the netw.</exception>
        /// <exception cref="IOException">Raised if the socket is closed.</exception>
        /// </summary>
        public virtual void Run()
        {
            try
            {
                StartListening();

                //waiting for connections
                try
                {
                    client = server.AcceptTcpClient();
                }
                catch (SocketException e)
                {
                    Logger.wr("Father closed program!");
                    //MessageBox.Show("Father closed program!");
                    throw e;
                }
                //connected!
                status = ServerConnectionStatus.Connected;
                while (!serverStop)
                {
                    if (client.Connected)
                        onReceive(this, new ByteBufferStream(client.GetStream()));
                }
                Logger.Slog("FINITO");
            }
            catch(Exception)
            {
                //CloseConnection();
                Logger.Slog("esco");
            }
        }

        /// <summary>
        /// Close the connection, stop the StartConnection Task (if threads are used).
        /// <exception cref="SocketException">Potentially raised by <c>ServerStop()</c>.</exception>
        /// </summary>
        public virtual void CloseClientConnection()
        {
            if (status == ServerConnectionStatus.Connected)
            {
                serverStop = true;
                // Shutdown and end connection
                client.Close();
                Logger.Slog("Close client connection");
                status = ServerConnectionStatus.Listening;
            }
        }

        public virtual void CloseConnection()
        {
            if( status == ServerConnectionStatus.Connected)
            {
                serverStop = true;
                // Shutdown and end connection
                client.Close();
                //closing the server
                server.Stop();
                Logger.Slog("Server stopped");
            }
            if(status == ServerConnectionStatus.Listening)
            {
                server.Stop();
                Logger.Slog("Server stop Listening");
            }
            status = ServerConnectionStatus.Disconnected;
        }

        protected virtual void OnReceive(EventArgs e)
        {
            onReceive?.Invoke(this, e);
        }
    }
}
