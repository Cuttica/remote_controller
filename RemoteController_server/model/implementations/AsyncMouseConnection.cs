﻿using System;
using System.Threading;

namespace RemoteController_server.model.implementations
{
    public class AsyncMouseConnection : UdpServerConnection
    {
        private Thread _thread;
        public AsyncMouseConnection(Int32 port) : base(port) { }
        

        public override void Run()
        {
            _thread = new Thread(base.Run);
            _thread.Start();
        }

        public override void CloseConnection()
        {
            if (Status == ServerConnectionStatus.Connected)
            {
                Stop = true;
                base.CloseConnection();
                _thread.Join();
            }else{
                base.CloseConnection();
            }
        }
    }
}