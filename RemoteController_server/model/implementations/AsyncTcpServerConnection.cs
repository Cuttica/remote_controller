﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Windows.Input;
using System.Drawing;
using RemoteController_server.model.implementations;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.Security;
using RemoteController_server.Debug;
using System.IO;
using System.Windows.Forms;
using System.Collections;
using System.Runtime.InteropServices;
using System.Threading;
using System.Collections.Concurrent;
using RemoteController_server.model.implementations.marshalling_utils;

namespace RemoteController_server.model.implementations
{
   static class StaticClass
    {
        public static async Task<T> WithWaitCancellation<T>(this Task<T> task, CancellationToken cancellationToken, T tmp)
        {
            // The tasck completion source. 
            var tcs = new TaskCompletionSource<bool>();

            // Register with the cancellation token.
            using (cancellationToken.Register(
                        s => ((TaskCompletionSource<bool>)s).TrySetResult(true), tcs))
                // If the task waited on is the cancellation token...
                if (task != await Task.WhenAny(task, tcs.Task))
                    //return
                    return tmp;

            // Wait for one or the other to complete.
            return await task;
        }
    }
    class AsyncTcpServerConnection : TcpServerConnection
    {
        public AsyncTcpServerConnection(RemoteConnection_server remoteConnection, IPAddress addr, int port) : base(remoteConnection, addr, port)
        {
        }

        protected CancellationTokenSource stopServerCancelSource;

        public override async void Run()
        {
            Logger.Slog("thread principale");
            await RunAsync();
        }

        async Task RunAsync()
        {
            StartListening();
            // The CancellationToken.
            stopServerCancelSource = new CancellationTokenSource();
            var cancellationToken = stopServerCancelSource.Token;
            base.serverStop = false;
            while (!base.serverStop)
            {
                //DA SISTEMARE CON TIMEOUT
                base.status = ServerConnectionStatus.Listening;
                Logger.Slog("AcceptTcpClientAsync called...");
                client = await server.AcceptTcpClientAsync().WithWaitCancellation(cancellationToken, null);
                if (stopServerCancelSource.IsCancellationRequested)
                {
                    Logger.Slog("Richiesta chiusura connessione dal server.");
                    server.Stop();
                    return;
                }
                //connected!
                base.status = ServerConnectionStatus.Connected;
                Logger.Slog("Connected!");
                base.clientStop = false;

                await ElaborateIncomingStream();
                
            }
            Logger.Slog("Server closing.");
            
            server.Stop();
        }

        protected virtual async Task ElaborateIncomingStream()
        {
            //do Some work
            client.GetStream();
        }


    }
}
