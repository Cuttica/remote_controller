﻿using System;
using System.Threading;

namespace RemoteController_server.model.implementations
{
    internal class AsyncKeyboardConnection : TcpServerConnection
    {
        private Thread _thread;
        public AsyncKeyboardConnection() : base() {}
        
        public AsyncKeyboardConnection(Int32 port) : base(port) { }

        public override void Run()
        {
            _thread = new Thread(base.Run);
            _thread.Start();
        }

        public override void CloseConnection()
        {
            if (Status == ServerConnectionStatus.Connected)
            {
                Stop = true;
                base.CloseConnection();
                Console.WriteLine("aspetto il figlio");
                _thread.Join();
                Console.WriteLine("è tornato");
            }else{
                base.CloseConnection();
                Console.WriteLine("aspetto il figlio");
                _thread.Join();
                Console.WriteLine("è tornato");
            }
        }
    }
}