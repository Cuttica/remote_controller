﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RemoteController_server.model
{
    interface IBuffer
    {
        byte[] Data { get; set; }
    }
}
