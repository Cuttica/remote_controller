﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RemoteController_server.model.implementations;

namespace RemoteController_server.model
{
    public delegate void DataHandler(Object sender, EventArgs e);
    public enum ServerConnectionStatus { Connected, Disconnected, Listening }
    interface IConnection
    {
        ServerConnectionStatus Status { get; }

        void Run();

        event DataHandler onReceive;

        void CloseConnection();
        void CloseClientConnection();

    }
}
