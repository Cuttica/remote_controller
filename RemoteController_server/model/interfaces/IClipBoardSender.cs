﻿using RemoteController_server.model.implementations.marshalling_utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RemoteController_server.model.interfaces
{
    public interface IClipboardSender
    {
        void SendClipboardData(AppData data);
    }
}
