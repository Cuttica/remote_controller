﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using RemoteController_server.model.implementations;
using System.Net;
using System.Net.Sockets;
using RemoteController_server.Debug;
using System.IO;
using System.Collections.Specialized;
using RemoteController_server.model.implementations.hook_utils;

namespace RemoteController_server
{
    public delegate void ShowClipboardAddressCallback(int port);
    public delegate void SetClipboardCallback(string format, object data);
    public delegate void ChangeNotifyIconCallback(bool isConnected);
    public delegate void SetVisibilityNotifyIconCallback(bool isConnected);
    public delegate void SetConsoleTextCallback(string msg, bool append);

    public partial class RemoteController_server : Form
    {
        /// <summary>
        /// Sent when the contents of the clipboard have changed.
        /// </summary>
        private const int WM_CLIPBOARDUPDATE = 0x031D;

        private RemoteServer server;

        public RemoteController_server()
        {
            Logger.Server = this;
            server = new RemoteServer(this);
            server.Password = "";
            //MouseKeyboardHookHandler_server.ScreenHeight = Screen.FromControl(this).Bounds.Height;
            //MouseKeyboardHookHandler_server.ScreenWidth = Screen.FromControl(this).Bounds.Width;
            InitializeComponent();
        }

        protected override void WndProc(ref Message m)
        {
            base.WndProc(ref m);

            if (m.Msg == WM_CLIPBOARDUPDATE)
            {
                server.ClipboardManager.ElaborateClipboard();
            }
        }


        private void RemoteController_server_Resize(object sender, EventArgs e)
        {
            //this.notifyIcon1.BalloonTipTitle = "Connected";
            //this.notifyIcon1.BalloonTipText = "Connection accepted";
            if (FormWindowState.Minimized == this.WindowState)
            {
                notifyIcon1.Visible = true;
                //notifyIcon1.ShowBalloonTip(500);
                this.Hide();
            }
            else if (FormWindowState.Normal == this.WindowState)
            {
                notifyIcon1.Visible = false;
            }
            
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            /*
            this.Hide();
            this.notifyIcon1.BalloonTipTitle = "Connected";
            this.notifyIcon1.BalloonTipText = "Connection accepted";
            notifyIcon1.Visible = true;
            notifyIcon1.ShowBalloonTip(500);
            */

            //if (server.Connection.Status == model.ServerConnectionStatus.Disconnected)
            //{
            //    server.Start();
            //    button2.Text = "Disconnect";
            //}
            //else if(server.Connection.Status == model.ServerConnectionStatus.Connected)
            //{
            //    server.Stop();
            //    button2.Text = "Connect Local";

            //}
            TcpListener tmps1 = new TcpListener(IPAddress.Any, 0);
            TcpListener tmps2 = new TcpListener(IPAddress.Any, 0);
            tmps1.Start();
            tmps2.Start();
            IPEndPoint end1 = ((IPEndPoint)tmps1.Server.LocalEndPoint);
            IPEndPoint end2 = ((IPEndPoint)tmps2.Server.LocalEndPoint);
            MessageBox.Show(end1.Address + ":" + end1.Port + " \n " + end2.Address + ":" + end2.Port);
        }
        private void Form1_FormClosing(Object sender, FormClosingEventArgs e)
        {
            if(server != null)
                server.Stop();
            notifyIcon1.Visible = false;
            notifyIcon1.Dispose();
        }

        private void button2_Click(object sender, EventArgs e)
        {

            if (server.Connection.Status == model.ServerConnectionStatus.Disconnected)
            {
                server.Start();
            }else if(server.Connection.Status == model.ServerConnectionStatus.Connected ||
                server.Connection.Status == model.ServerConnectionStatus.Listening)
            {
                server.Stop();
            }

        }

        public void ShowClipboardAddress(int port)
        {
            if (label1.InvokeRequired)
            {
                ShowClipboardAddressCallback d = new ShowClipboardAddressCallback(ShowClipboardAddress);
                this.Invoke(d, new object[] { port });
            }else
            {
                if (port == -1)
                {
                    SetIcon(false);
                    label1.Text = "Disconnected";
                    button2.Text = "Connect Local";
                }
                else
                {
                    string txt = "Waiting for connection on:\n\n";
                    foreach (string s in this.server.GetLocalIPAddresses())
                    {
                        txt += s + ":" + port + "\n";
                    }
                    label1.Text = txt;
                    button2.Text = "Disconnect";
                }
            }
        }
        private void SetIcon(bool onFocus)
        {
            if (onFocus)
            {
                notifyIcon1.Visible = false;
                notifyIcon1.Icon = new Icon(Path.GetFullPath(".\\..\\..\\img\\onFocusIcon.ico"));
                notifyIcon1.Visible = true;
            }
            else
            {
                notifyIcon1.Visible = false;
                notifyIcon1.Icon = new Icon(Path.GetFullPath(".\\..\\..\\img\\notOnFocusIcon.ico"));
                notifyIcon1.Visible = true;
            }
        }

        public void ChangeNotifyIconState(bool isConnected)
        {
            if (label1.InvokeRequired)
            {
                ChangeNotifyIconCallback d = new ChangeNotifyIconCallback(ChangeNotifyIconState);
                this.Invoke(d, new object[] { isConnected });
            }else
            {
                SetIcon(isConnected);
            }
        }
        public void SetVisibilityNotifyIcon(bool visible)
        {
            if (label1.InvokeRequired)
            {
                SetVisibilityNotifyIconCallback d = new SetVisibilityNotifyIconCallback(ChangeNotifyIconState);
                this.Invoke(d, new object[] { visible });
            }
            else
            {
                if (visible)
                {
                    SetIcon(false);
                    this.Show();
                }
                else
                {
                    SetIcon(true);
                    this.notifyIcon1.BalloonTipTitle = "Connected";
                    this.notifyIcon1.BalloonTipText = "Connection accepted";
                    notifyIcon1.Visible = true;
                    notifyIcon1.ShowBalloonTip(500);
                    this.Hide();
                }
            }
        }

        public void SetClipboardData(string format, object data)
        {
            if (label1.InvokeRequired)
            {
                SetClipboardCallback d = new SetClipboardCallback(SetClipboardData);
                this.Invoke(d, new object[] { format, data });
            }
            else
            {
                Logger.Slog("stoppo il c listener");
                server.ClipboardManager.Stop();
                Logger.Slog("inserisco in c");
                if (format.CompareTo(DataFormats.FileDrop) == 0)
                {
                    Clipboard.SetFileDropList((StringCollection)data);
                }
                else
                {
                    Clipboard.SetData(format, data);
                }
                Logger.Slog("riattio il c listener");
                server.ClipboardManager.Start();
            }
        }

        CancellationTokenSource source = new CancellationTokenSource();

        private async void button1_Click_1(object sender, EventArgs e)
        {
            //CancellationToken ct = source.Token;
            //string ris = await doSomethingAsync().WithWaitCancellation<string>(ct,"tmp");
            //Console.WriteLine("TERMINO");
            Logger.Slog(""+Screen.FromControl(this).Bounds.Width);
            Logger.Slog("" + Screen.FromControl(this).Bounds.Height);
            Logger.Slog("" + MouseKeyboardHookHandler_server.ScreenWidth);
            Logger.Slog("" + MouseKeyboardHookHandler_server.ScreenHeight);

        }

        private void mouse_onmove(object sender, EventArgs e)
        {
        }
        private async Task<string> doSomethingAsync()
        {
            return await Task.Run(() =>
            {
                while (true)
                {
                    Thread.Sleep(1000);
                    Console.WriteLine("ciaone");
                }
                return "end";
            });     
        }
        public class MyException : Exception { }

        private void button3_Click(object sender, EventArgs e)
        {
            source.Cancel();
        }

        public void SetConsoleText(string msg, bool append)
        {
            if (richTextBox1.InvokeRequired)
            {
                SetConsoleTextCallback d = new SetConsoleTextCallback(SetConsoleText);
                this.Invoke(d, new object[] { msg, append});
            }else
            {
                if (append)
                {
                    richTextBox1.Text += msg;
                }else
                {
                    richTextBox1.Text = msg;
                }
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            server.Password = textBox1.Text;
        }
        //private void button1_Click_1(object sender, EventArgs e)
        //{
        //    doSomething();
        //    int count = 0;
        //    while (count++ < 10)
        //    {
        //        Logger.Slog(".");
        //        Thread.Sleep(500);

        //    }
        //}

        //private async void doSomething()
        //{
        //    await Task.Run(() =>
        //    {
        //        Thread.Sleep(1000);
        //        Logger.Slog("termino");
        //    });

        //    Logger.Slog("riprendo da qui? ma poi a fare cheeeee????");
        //}
    }
}
